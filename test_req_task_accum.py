##########################################################################
# test_req_task_accum.py
#
# Copyright (C) 2018
# Associated Universities, Inc. Washington DC, USA.
#
# This script is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# [Add the link to the JIRA ticket here once it exists]
#
# Based on the requirements listed in plone found here:
# https://casa.nrao.edu/casadocs-devel/stable/global-task-list/task_accum/about
#
#
##########################################################################

CASA6 = False
try:
    import casatools
    from casatasks import accum, gaincal, calstat
    CASA6 = True
    tb = casatools.table()
except ImportError:
    from __main__ import default
    from tasks import *
    from taskinit import *
import sys
import os
import unittest
import shutil
import numpy as np
from filecmp import dircmp

### DATA ###

if CASA6:
    datapath = casatools.ctsys.resolve('visibilities/vla/ngc5921.ms/')
    datapath2 = casatools.ctsys.resolve('visibilities/vla/ngc7538_ut.ms')
    #datapath2 = casatools.ctsys.resolve('visibilities/vla/gaincaltest2.ms')

else:
    if os.path.exists(os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req'):
        datapath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/vla/ngc5921.ms/'
        datapath2 = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/vla/ngc7538_ut.ms/'
        #datapath2 = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/vla/gaincaltest2.ms/'
        
    else:
        datapath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/vla/ngc5921.ms/'
        datapath2 = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/vla/ngc7538_ut.ms/'
        #datapath2 = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/vla/gaincaltest2.ms/'
        

# Calibration tables to use for this test 
calTable1 = 'testcal1.gcal'
calTable2 = 'testcal2.gcal'
calTabSpw = 'testcalspw.gcal'

output = 'out.cal'
output2 = 'out2.cal'
output3 = 'out3.cal'

datacopy = 'accum_data.ms'
datacopy2 = 'accum_data2.ms'

def change_perms(path):
    os.chmod(path, 0o777)
    for root, dirs, files in os.walk(path):
        for d in dirs:
            os.chmod(os.path.join(root,d), 0o777)
        for f in files:
            os.chmod(os.path.join(root,f), 0o777)

class accum_test(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        shutil.copytree(datapath, datacopy)
        shutil.copytree(datapath2, datacopy2)
        change_perms(datacopy)
        change_perms(datacopy2)
        
        gaincal(vis=datacopy, caltable=calTable1)
        gaincal(vis=datacopy, caltable=calTable2, calmode='p')
        gaincal(vis=datacopy2, caltable=calTabSpw)
    
    def setUp(self):
        if not CASA6:
            default(accum)
            
    def tearDown(self):
        if os.path.exists(output):
            shutil.rmtree(output)
            
        if os.path.exists(output2):
            shutil.rmtree(output2)
            
        if os.path.exists(output3):
            shutil.rmtree(output3)
        
    
    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(datacopy)
        shutil.rmtree(datacopy2)
    
        shutil.rmtree(calTable1)
        shutil.rmtree(calTable2)
        shutil.rmtree(calTabSpw)
    
    def test_takesMS(self):
        '''
            test_takesMS
            --------------
            
            Check that the task takes a valid ms and produces an output
        '''
        
        accum(vis=datacopy, incrtable=calTable1, caltable=output)
        self.assertTrue(os.path.exists(output))
        
        
    def test_tableIn(self):
        '''
            test_tableIn
            -----------------
            
            Check that multiple run will combine additional calibration tables
        '''
        
        accum(vis=datacopy, incrtable=calTable1, caltable=output)
        accum(vis=datacopy, tablein=output, incrtable=calTable2, caltable=output2)
        
        dcmp = dircmp(output, output2)
        self.assertTrue(len(dcmp.diff_files) > 1)
        

    def test_accumtime(self):
        '''
            test_accumtime
            ----------------
            
            Check that the accumtime parameter sets the time seperation when making tablein
            
        '''
        
        #accum(vis=datapath, incrtable=calTable1, caltable=output)
        
        accum(vis=datacopy,incrtable=calTable1, caltable=output)
        accum(vis=datacopy, accumtime=1000.0, incrtable=calTable1, caltable=output2)
        
        statOut = calstat(output, datacolumn='CPARAM')
        statOut2 = calstat(output2, datacolumn='CPARAM')
        
        self.assertFalse(statOut == statOut2)
        
    def test_incrtable(self):
        '''
            test_incrtable
            ----------------
            
            Check that incrtable paramter selects the calibration data to be interpolated into the tablein file
            This parameter must be specified
        '''
        
        accum(vis=datacopy, incrtable=calTable1, caltable=output)
        accum(vis=datacopy, incrtable=calTable2, caltable=output2)
        
        statOut1 = calstat(output, datacolumn='CPARAM')
        statOut2 = calstat(output2, datacolumn='CPARAM')
        
        self.assertFalse(statOut1 == statOut2)
        
    def test_caltable(self):
        '''
            test_caltable
            ---------------
            
            Check that this parameter specifies the output calibration file
            If no value is passed then the tablein file will be used
        '''
        
        accum(vis=datacopy, incrtable=calTable1, caltable=output)
        self.assertTrue(os.path.exists(output))
        
    def test_field(self):
        '''
            test_field
            ------------
            
            Check that the field parameter selects field ids or names for tablein to process
        '''
        
        accum(vis=datacopy, incrtable=calTable1, caltable=output)
        accum(vis=datacopy, incrtable=calTable1, field='1', caltable=output2)
        
        statOut1 = calstat(output, datacolumn='CPARAM')
        statOut2 = calstat(output2, datacolumn='CPARAM')
        
        self.assertFalse(statOut1 == statOut2)
        
    def test_calfield(self):
        '''
            test_calfield
            ---------------
            
            Check that the calfield parameter selects fields from the incrtable to process
        '''
        
        accum(vis=datacopy, incrtable=calTable1, caltable=output)
        accum(vis=datacopy, incrtable=calTable1, calfield='1', caltable=output2)
        
        statOut1 = calstat(output, datacolumn='CPARAM')
        statOut2 = calstat(output2, datacolumn='CPARAM')
        
        self.assertFalse(statOut1 == statOut2)
        
    def test_interp(self):
        '''
            test_interp
            -------------
            
            Check that the interp parameter selects the interpolation type to use for each gaintable
        '''
        
        accum(vis=datacopy, incrtable=calTable1, caltable=output)
        accum(vis=datacopy, incrtable=calTable1, interp='cubic', caltable=output2)
        
        statOut1 = calstat(output, datacolumn='CPARAM')
        statOut2 = calstat(output2, datacolumn='CPARAM')
        
        self.assertFalse(statOut1 == statOut2)
        
        
    # Failure related to CAS-4240
    def test_spwmap(self):
        '''
            test_spwmap
            ----------
            
            Check that spw parameter selects the spectral window combinations to form gaintable(s)
            default is to apply solutions from each spw to that spw only
        '''
        
        #accum(vis=datapath2, incrtable=calTabSpw, caltable=output)
        accum(vis=datacopy2, incrtable=calTabSpw, spwmap=[0,0], caltable=output2)
        
        tb.open(output2)
        result = np.mean(tb.getcol('CPARAM'))
        tb.close()
        
        print(result)
        
        self.fail()
        statOut1 = calstat(output, datacolumn='CPARAM')
        statOut2 = calstat(output2, datacolumn='CPARAM')
        
        self.assertFalse(statOut1 == statOut2)
    
    
def suite():
    return[accum_test]


if __name__ == '__main__':
    unittest.main()
