##########################################################################
# test_req_task_ft.py
#
# Copyright (C) 2018
# Associated Universities, Inc. Washington DC, USA.
#
# This script is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# [Add the link to the JIRA ticket here once it exists]
#
# Based on the requirements listed in plone found here:
# https://casa.nrao.edu/casadocs/casa-5.4.0/global-task-list/task_ft/about
#
#
##########################################################################

CASA6 = False
try:
    import casatools
    from casatasks import ft, rmtables, casalog, clearcal
    tb = casatools.table()
    cl = casatools.componentlist()
    CASA6 = True
except ImportError:
    from __main__ import default
    from tasks import *
    from taskinit import *
    from casac import casac
    cl = casac.componentlist()
import sys
import os
import unittest
import numpy as np
import shutil
import glob

if CASA6:
    datapath = casatools.ctsys.resolve('visibilities/other/outlier_ut.ms')
    calpath = casatools.ctsys.resolve(os.path.join(os.path.dirname(os.path.abspath(casatools.__file__)),'__data__/nrao/VLA/CalModels/3C138_K.im'))
    multipath = casatools.ctsys.resolve('image/inmodel1.model.tt0')
else:
    if os.path.exists(os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/other/outlier_ut.ms/'):
        datapath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/other/outlier_ut.ms/'
        calpath = os.environ.get('CASAPATH').split()[0] + '/data/nrao/VLA/CalModels/3C138_K.im'
        multipath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/image/inmodel1.model.tt0'
        
    else:
        datapath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/other/outlier_ut.ms/'
        calpath = os.environ.get('CASAPATH').split()[0] + '/data/nrao/VLA/CalModels/3C138_K.im'
        multipath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/image/inmodel1.model.tt0'
datacopy = 'outlier_ut.ms'
filepath = 'testlog.log'
    
logpath = casalog.logfile()
    
class ft_test(unittest.TestCase):
    
    def setUp(self):
        if not CASA6:
            default(ft)
        shutil.copytree(datapath, datacopy)
        os.chmod(datacopy, 493)
        for root, dirs, files in os.walk(datacopy):
            for d in dirs:
                os.chmod(os.path.join(root, d), 493)
            for f in files:
                os.chmod(os.path.join(root, f), 493)
    
    def tearDown(self):
        rmtables(datacopy)
        rmtables('complist.cl')
        casalog.setlogfile(logpath)
        if os.path.exists(filepath):
            os.remove(filepath)
    
    
    def testopenMS(self):
        '''test open MS: Check that a MS can be opened and used'''
        casalog.setlogfile('testlog.log')
        ft(datacopy, model=calpath)
        self.assertFalse('SEVERE' in open('testlog.log').read())
        
        if CASA6:
            with self.assertRaises(AssertionError):
                ft('fake.ms')
        else:
            ft('fake.ms')
            self.assertTrue('SEVERE' in open('testlog.log').read())
    
    def test_changemodelScratch(self):
        '''test change model scratch: Check that the scratch column model data is chaged'''
        tb.open(datacopy)
        before = tb.getcol('MODEL_DATA')
        ft(datacopy, model=calpath, usescratch=True)
        after = tb.getcol('MODEL_DATA')
        tb.close()
        self.assertFalse(np.all(before == after))
    
    def test_changemodelHead(self):
        '''test change model the head: Check that the info in the header is changed'''
        self.assertTrue(len(glob.glob(datacopy + r'/SOURCE/FT_MODEL*')) == 0)
        ft(datacopy, model=calpath)
        self.assertTrue(len(glob.glob(datacopy + r'/SOURCE/FT_MODEL*')) == 1)
        
    def test_stdgridder(self):
        #?
        pass
    
    def test_multiterm(self):
        '''test multiterm: test using a multiterm image'''
        casalog.setlogfile('testlog.log')
        ft(datacopy, model=multipath)
        self.assertFalse('SEVERE' in open('testlog.log').read())
    
    def test_incremental(self):
        '''test incremental: Check that incremental adds another model to the source dir'''
        # Make the first model 
        ft(datacopy, model=calpath)
        before = len([name for name in os.listdir(datacopy + '/SOURCE') if os.path.isdir(os.path.join(datacopy + '/SOURCE', name))])
        # Make the second model using incremental
        ft(datacopy, model=calpath, incremental=True)
        after = len([name for name in os.listdir(datacopy + '/SOURCE') if os.path.isdir(os.path.join(datacopy + '/SOURCE', name))])
        # Check that the first model was not replaced
        self.assertTrue(after > before)
        
    def test_usemodel(self):
        '''test use model: when given model and complist, use the model'''
        cl.addcomponent(shape='point', flux=13.4, fluxunit='Jy', spectrumtype='spectral index', index=(-0.8), freq='1.25GHz', dir='J2000 12h33m45.3s -23d01m11.2s')
        # save the component list under the name 'mycomplist.cl'
        cl.rename('complist.cl')
        cl.close()
        # attempt with just model
        ft(datacopy, model=calpath, usescratch=True)
        tb.open(datacopy)
        justModel = tb.getcol('MODEL_DATA')[0, 0, 0]
        
        # attempt with just the complist
        ft(vis='outlier_ut.ms/', complist='complist.cl', usescratch=True)
        
        justComplist = tb.getcol('MODEL_DATA')[0, 0, 0]
        
        # Check that they are not the same
        self.assertFalse(justModel == justComplist)
        # use ft with both
        ft(datacopy, model=calpath, complist='complist.cl', usescratch=True)

        bothModComp = tb.getcol('MODEL_DATA')[0, 0, 0]
        tb.close()
        # Check that it will use the model over the complist
        self.assertTrue(bothModComp == justModel)
    
    def test_spwselect(self):
        '''test spw select: Check to see that the spw selection works'''
        casalog.setlogfile('testlog.log')
        ft(datacopy, model=calpath, spw='0')
        self.assertFalse('SEVERE' in open('testlog.log').read())
        
        ft(datacopy, model=calpath, spw='1')
        self.assertTrue('SEVERE' in open('testlog.log').read())

    def test_fieldselect(self):
        '''test field select: Check to see that the field selection works'''
        casalog.setlogfile('testlog.log')
        ft(datacopy, model=calpath, field='0')
        self.assertFalse('SEVERE' in open('testlog.log').read())
        
        ft(datacopy, model=calpath, field='1')
        self.assertTrue('SEVERE' in open('testlog.log').read())
    
    def test_model(self):
        '''test model: Check that the model param only selects real model'''
        casalog.setlogfile('testlog.log')
        # Test using a fake
        rmtables(datacopy)
        if CASA6:
            with self.assertRaises(Exception):
                ft(datacopy, model='FAKE.im')
               
        else:
            #ft(datacopy, model='FAKE.im')
            #self.assertTrue('SEVERE' in open('testlog.log').read())
            pass
    
    def test_nterms(self):
        '''test nterms: test using multiple terms and make sure several models are generated'''
        # use nterms to make multiple models
        ft(datacopy, model=[calpath, calpath], nterms=2)
        after = len([name for name in os.listdir(datacopy + '/SOURCE') if os.path.isdir(os.path.join(datacopy + '/SOURCE', name))])
        # Check that it made more than 1 models
        self.assertTrue(after > 1)
    
    def test_rffreq(self):
        #?
        pass
    
def suite():
    return[ft_test]

if __name__ == '__main__':
    unittest.main()
