##########################################################################
# test_req_task_polcal.py
#
# Copyright (C) 2018
# Associated Universities, Inc. Washington DC, USA.
#
# This script is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# [Add the link to the JIRA ticket here once it exists]
#
# Based on the requirements listed in plone found here:
# https://casa.nrao.edu/casadocs-devel/stable/global-task-list/task_polcal/about
#
#
##########################################################################

CASA6 = False
try:
    import casatools
    from casatasks import polcal, casalog
    CASA6 = True
except ImportError:
    from __main__ import default
    from tasks import *
    from taskinit import *
import sys
import os
import unittest
import shutil
import numpy as np



### DATA ###

if CASA6:
    datapath = casatools.ctsys.resolve('visibilities/vla/ngc5921.ms/')
    spwpath = casatools.ctsys.resolve('visibilities/vla/ngc7538_ut.ms/')
    intentpath = casatools.ctsys.resolve('visibilities/evla/CAS-6733.ms/')
    gainpath = casatools.ctsys.resolve('caltables/ngc5921.ref1a.gcal')
    libfile = casatools.ctsys.resolve('text/testcallib.txt')
    tablecompdata = casatools.ctsys.resolve('visibilities/vla/gaincaltest2.ms')
    callibfile = casatools.ctsys.resolve('text/refcallib.txt')
    tCal = casatools.ctsys.resolve('caltables/gaincaltest2.ms.T0')
    gCal = casatools.ctsys.resolve('caltables/gaincaltest2.ms.G0')

    tb = casatools.table()
    
else:
    if os.path.exists(os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req'):
        datapath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/vla/ngc5921.ms/'
        spwpath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/vla/ngc7538_ut.ms/'
        intentpath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/evla/CAS-6733.ms/'
        gainpath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/caltables/ngc5921.ref1a.gcal/'
        libfile = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/text/testcallib.txt/'
        tablecompdata = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/vla/gaincaltest2.ms/'
        callibfile = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/text/refcallib.txt'
        tCal = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/caltables/gaincaltest2.ms.T0'
        gCal = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/caltables/gaincaltest2.ms.G0'
        
    else:
        datapath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/vla/ngc5921.ms/'
        spwpath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/vla/ngc7538_ut.ms/'
        intentpath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/evla/CAS-6733.ms/'
        gainpath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/caltables/ngc5921.ref1a.gcal/'
        libfile = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/text/testcallib.txt/'
        tablecompdata = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/vla/gaincaltest2.ms/'
        callibfile = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/text/refcallib.txt'
        tCal = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/caltables/gaincaltest2.ms.T0'
        gCal = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/caltables/gaincaltest2.ms.G0'
    

def getparam(caltable, colname='CPARAM'):
    ''' Open a caltable and get the provided column '''

    tb.open(caltable)
    outtable = tb.getcol(colname)
    tb.close()

    return outtable

def tableComp(table1, table2, cols=[], rtol=8e-7, atol=1e-8):
    ''' Compare two caltables '''

    tableVal1 = {}
    tableVal2 = {}

    tb.open(table1)
    colname1 = tb.colnames()

    for col in colname1:
        try:
            tableVal1[col] = tb.getcol(col)
        except RuntimeError:
            pass
    tb.close()

    tb.open(table2)
    colname2 = tb.colnames()

    for col in colname2:
        try:
            tableVal2[col] = tb.getcol(col)
        except RuntimeError:
            pass
    tb.close()

    truthDict = {}

    for col in tableVal1.keys():

        try:
            truthDict[col] = np.isclose(tableVal1[col], tableVal2[col], rtol=rtol, atol=atol)
        except TypeError:
            print(col, 'ERROR in finding truth value')
            casalog.post(message=col+': ERROR in determining the truth value')

    if len(cols) == 0:
        
        truths = [[x, np.all(truthDict[x] == True)] for x in truthDict.keys()]

    else:

        truths = [[x, np.all(truthDict[x] == True)] for x in cols]

    return np.array(truths)
        
        
calpath = 'testcal.cal'
calpath2 = 'testcal2.cal'

logpath = casalog.logfile()
testlog = 'testlog.log'
temptCal = 'temptcal.T0'
        
class polcal_test(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        pass
    
    def setUp(self):
        pass
    
    def tearDown(self):
        casalog.setlogfile(logpath)
        
        if os.path.exists(calpath):
            shutil.rmtree(calpath)
            
        if os.path.exists(calpath2):
            shutil.rmtree(calpath2)
            
        if os.path.exists('cal.B'):
            shutil.rmtree('cal.B')
            
        if os.path.exists(testlog):
            os.remove(testlog)
            
        if os.path.exists(temptCal):
            shutil.rmtree(temptCal)
    
    @classmethod
    def tearDownClass(cls):
        pass
    
    def test_takesVis(self):
        '''
            test_takesVis
            ---------------
            
            Check that the task takes a valid ms
        '''
        
        casalog.setlogfile(testlog)
        polcal(vis=datapath, caltable=calpath, poltype='Xf')
        
        self.assertTrue('SEVERE' not in open(testlog).read())
        
        if CASA6:
            with self.assertRaises(Exception):
                polcal(vis=datapath, poltype='Xf')
        else:
            polcal(vis=datapath, poltype='Xf')
            self.assertTrue('SEVERE' in open(testlog).read())
            
    def test_makesCal(self):
        '''
            test_makesCal
            ---------------
            
            Check that the caltable parameter gives the name to be passed to the caltable
        '''
        
        polcal(vis=datapath, caltable=calpath, poltype='Xf')
        
        self.assertTrue(os.path.exists(calpath))
        
        
    def test_field(self):
        '''
            test_field
            ------------
            
            Check that the field selection parameter properly selects a subset of the data
        '''
        
        polcal(vis=datapath, caltable=calpath, poltype='Xf', field='1')
        
        field_id = getparam(calpath, 'FIELD_ID')
        
        self.assertTrue(np.all(field_id == 1))
        
    def test_spw(self):
        '''
            test_spw
            ----------
            
            Check that the spw selection parameter properly selects a subset of the data
        '''
        
        polcal(vis=spwpath, caltable=calpath, poltype='Xf', spw='1')
        
        spw_id = getparam(calpath, 'SPECTRAL_WINDOW_ID')
        
        self.assertTrue(np.all(spw_id == 1))
        
    def test_intent(self):
        '''
            test_intent
            -------------
            
            Check that the intent selection parameter properly selects a subset of the data
        '''
        
        polcal(vis=intentpath, caltable=calpath, poltype='Xf')
        polcal(vis=intentpath, caltable=calpath2, poltype='Xf', intent='OBSERVE_TARGET#UNSPECIFIED')
        
        noIntent = len(getparam(calpath, 'TIME'))
        withIntent = len(getparam(calpath2, 'TIME'))
        
        self.assertFalse(withIntent > noIntent)
        
        
    def test_selectData(self):
        '''
            test_selectdata
            -----------------
            
            check that the selectdata parameter allows for the use of additional selection parameters
        '''
        
        polcal(vis=datapath, caltable=calpath, poltype='Xf', selectdata=False, scan='10')
        
        selectedData = getparam(calpath, 'SCAN_NUMBER')
        
        self.assertFalse(np.all(selectedData == 10))
        
    def test_antenna(self):
        '''
            test_antenna
            --------------
            
            Check that the antenna parameter selects a subset of the data
        '''
        
        polcal(vis=tablecompdata, caltable=calpath, poltype='Df', antenna='0~5&')
        polcal(vis=tablecompdata, caltable=calpath2, poltype='Df')
        
        withAnt = np.sum(getparam(calpath, 'FLAG'))
        noAnt = np.sum(getparam(calpath2, 'FLAG'))
        
        self.assertTrue(withAnt > noAnt)
        
    def test_timerange(self):
        '''
            test_timerange
            ----------------
            
            Check that the antenna parameter selects a subset of the data
        '''
        
        polcal(vis=tablecompdata, caltable=calpath, poltype='Df', timerange='4:33:23~4:38:23')
        polcal(vis=tablecompdata, caltable=calpath2, poltype='Df')
        
        withTime = len(getparam(calpath, 'TIME'))
        noTime = len(getparam(calpath2, 'TIME'))
        
        self.assertTrue(withTime < noTime)
        
    def test_uvrange(self):
        '''
            test_uvrange
            --------------
            
            Check that the uvrange parameter selects a subset of the data
        '''
        
        polcal(vis=datapath, caltable=calpath, poltype='Xf', uvrange='1~34')
        polcal(vis=datapath, caltable=calpath2, poltype='Xf')
        
        withUV = np.shape(getparam(calpath))[2]
        noUV = np.shape(getparam(calpath2))[2]
        
        self.assertTrue(withUV < noUV)
        
        
    def test_scan(self):
        '''
            test_scan
            -----------
            
            Check that the scan parameter selects a subset of the data
        '''
        
        polcal(vis=datapath, caltable=calpath, poltype='Xf', scan='1')

        scanNumbers = getparam(calpath, 'SCAN_NUMBER')
        
        self.assertTrue(np.all(scanNumbers == 1))
        
    def test_observation(self):
        '''
            test_observation
            ------------------
            
            Check that the observation parameter selects a subset of the data
        '''
        
        #TODO: Come back to this one. I'll need more data
        
        polcal(vis=datapath, caltable=calpath, poltype='Xf', observation='0')
        
        self.assertTrue(os.path.exists(calpath))
        
    def test_solint(self):
        '''
            test_solint
            -------------
            
            Check that the solint parameter defines the solution interval of the function
        '''
        
        polcal(vis=datapath, caltable=calpath, poltype='Xf',solint='10s')
        polcal(vis=datapath, caltable=calpath2, poltype='Xf')
        
        withsolint = len(getparam(calpath, 'TIME'))
        nosolint = len(getparam(calpath2, 'TIME'))
        
        self.assertTrue(withsolint > nosolint)
        
    def test_combine(self):
        '''
            test_combine
            --------------
            
            Check that the combine parameter gives which axes to combine for solve
        '''
        
        polcal(vis=datapath, caltable=calpath, poltype='Xf',combine='obs,scan,field')
        
        withUV = len(getparam(calpath, 'TIME'))
        
        self.assertTrue(withUV == 28)
        
        
    def test_preavg(self):
        '''
            test_preavg
            -------------
            
            Check that this parameter chages the pre-averaging interval
        '''
        
        polcal(vis=tablecompdata, caltable=calpath, poltype='Df', preavg=10)
        polcal(vis=tablecompdata, caltable=calpath2, poltype='Df')
        
        self.assertFalse(np.all(tableComp(calpath, calpath2)[:,1] == 'True'))
        
    def test_refant(self):
        '''
            test_refant
            -------------
            
            Check that this parameter sets the reference antenna name
            TODO again I don't know exactly what numerical effect this should have on the data
        '''
        
        polcal(vis=tablecompdata, caltable=calpath, poltype='Df', refant='2')
        
        refants = getparam(calpath, 'ANTENNA2')
        
        self.assertTrue(np.all(refants == 2))
        
    def test_minblperant(self):
        '''
            test_minblperant
            ------------------
            
            Check that the minblperant parameter provides the minimum baselines per antenna required for solve
            TODO: again I don't know exactly what effects this should be having on the data
        '''
        
        polcal(vis=tablecompdata, caltable=calpath, poltype='Df', minblperant=20)
        
        self.assertFalse(os.path.exists(calpath))
        
    def test_minsnr(self):
        '''
            test_minsnr
            -------------
            
            Check that this parameter rejects solutions below a given SNR
        '''
        
        polcal(vis=tablecompdata, caltable=calpath, poltype='Df', minsnr=180)
        flagged = np.sum(getparam(calpath, 'FLAG'))
        
        self.assertTrue(flagged == 4857)
        
    def test_poltype(self):
        '''
            test_poltype
            --------------
            
            Check that the selected type of instrumental polarization solution has the desired effect
            Allowed values(D, Df, D+X, Df+X, D+QU, Df+QU, Dgen, Dfgen, Dgen+X, Dfgen+X, Dgen+QU, Dfgen+QU, Dlls, Dflls, X, Xf, Xj)
        '''
        
        #TODO come back to this one, it will likely end up as several different tests
        
        polcal(vis=tablecompdata, caltable=calpath, poltype='Df')
        polcal(vis=tablecompdata, caltable=calpath2, poltype='Df+X')
        
        self.assertFalse(np.all(tableComp(calpath, calpath2)[:,1] == 'True'))
        
    def test_poltypeDf(self):
        '''
            test_poltypeDf
            ----------------
            
            Check that the poltype value of Df gives the expected solution
        '''
        
        polcal(vis=tablecompdata, caltable=calpath, poltype='Df')
        datamean = np.mean(getparam(calpath))
        
        self.assertTrue(datamean == -2.0866637260659396e-05-4.3762559748303367e-07j)
        
    def test_poltypeDfX(self):
        '''
            test_poltypeDfX
            -----------------
            
            Check that the poltype value of Df+X gives the expected solution
        '''
        
        polcal(vis=tablecompdata, caltable=calpath, poltype='Df+X')
        datamean = np.mean(getparam(calpath))
        
        self.assertTrue(datamean == -2.0865695329093844e-05-3.231090864895338e-07j)
        
    def test_poltypeDfQU(self):
        '''
            test_poltypeDfQU
            -----------------
            
            Check that the poltype value of Df+QU gives the expected solution
        '''
        
        polcal(vis=tablecompdata, caltable=calpath, poltype='Df+QU')
        datamean = np.mean(getparam(calpath))
        
        self.assertTrue(datamean == -0.52794535715473523-0.047769018522517401j)
        
    def test_poltypeXf(self):
        '''
            test_poltypeXf
            -----------------
            
            Check that the poltype value of Xf gives the expected solution
        '''
        
        polcal(vis=tablecompdata, caltable=calpath, poltype='Xf')
        datamean = np.mean(getparam(calpath))
        
        self.assertTrue(datamean == 0.023335894603535835+0.02280737559097664j)
        
    def test_poltypeDFlls(self):
        '''
            test_poltypeXf
            -----------------
            
            Check that the poltype value of Dflls gives the expected solution
        '''
        
        polcal(vis=tablecompdata, caltable=calpath, poltype='Dflls')
        datamean = np.mean(getparam(calpath))
        
        self.assertTrue(datamean == -2.0857481758427913e-05+2.0908864772225149e-05j)
    """    
    def test_poltypeXfparangQU(self):
        '''
            test_poltypeXfparangQU
            -----------------
            
            Check that the poltype value of Xfparang+QU gives the expected solution
        '''
        #TODO revisit
        
        polcal(vis=tablecompdata, caltable=calpath, poltype='Xfparang+QU')
        datamean = np.mean(getparam(calpath))
        
        self.fail(msg='encounters a SEVER ERROR')
        
        #self.assertTrue(datamean == )
    """
    def test_poltypePosAng(self):
        '''
            test_poltypePosAng
            -----------------
            
            Check that the poltype value of PosAng gives the expected solution
        '''
        
        polcal(vis=tablecompdata, caltable=calpath, poltype='PosAng')
        tb.open(calpath)
        datacols = tb.colnames()
        tb.close()
        
        self.assertTrue('FPARAM' in datacols)
        
        
    def test_channelized(self):
        '''
            test_channelized
            ------------------
            
            Check if the addition of 'f' to the poltype gives the channelized solution
        '''
        
        polcal(vis=tablecompdata, caltable=calpath, poltype='Df')
        channelized = np.shape(getparam(calpath))[1]
        
        self.assertTrue(channelized == 8)
        
    
    
    def test_smodel(self):
        '''
            test_smodel
            -------------
            
            Check that this parameter provides the Point source stokes parameters for source model
            TODO: I can't find what exactly this is doing to the data, so this is a placeholder
        '''
        
        polcal(vis=tablecompdata, caltable=calpath, poltype='Df', smodel=[1.0,0.11,0.0,0.0])
        polcal(vis=tablecompdata, caltable=calpath2, poltype='Df', smodel=[1.0,0.22,0.0,0.0])
        
        self.assertFalse(np.all(tableComp(calpath,calpath2)[:,1] == 'True'))
        
    def test_append(self):
        '''
            test_append
            -------------
            
            Check that solutions are appended to an existing table
        '''
        
        polcal(vis=spwpath, caltable=calpath, poltype='Xf', spw='0')
        sizeOrig = np.shape(getparam(calpath, 'SPECTRAL_WINDOW_ID'))[0]
        
        polcal(vis=spwpath, caltable=calpath, poltype='Xf', spw='1', append=True)
        sizeAppend = np.shape(getparam(calpath, 'SPECTRAL_WINDOW_ID'))[0]
        
        self.assertTrue(sizeAppend > sizeOrig)
        
    def test_callib(self):
        '''
            test_callib
            -------------
            
            Check that the callib is applied
        '''
        
        shutil.copytree(tCal, temptCal)
        
        polcal(vis=tablecompdata, caltable=calpath, poltype='Df', docallib=True, callib=callibfile)
        polcal(vis=tablecompdata, caltable=calpath2, poltype='Df', docallib=False)
        
        self.assertFalse(np.all(tableComp(calpath, calpath2)[:,1] == 'True'))
        
        
    def test_gaintable(self):
        '''
            test_gaintable
            ----------------
            
            Check that the gaintables are applied
            TODO come back and add some level of validation
        '''
        
        polcal(vis=tablecompdata, caltable=calpath, poltype='Df', gaintable=[tCal])
        polcal(vis=tablecompdata, caltable=calpath2, poltype='Df')
        
        self.assertFalse(np.all(tableComp(calpath, calpath2)[:,1] == 'True'))
        
    def test_gainfield(self):
        '''
            test_gainfield
            ----------------
            
            Check that this parameter selects a subset of calibrators from the gaintables
            TODO come back to this and give improvements
        '''
        
        polcal(vis=tablecompdata, caltable=calpath, poltype='Df', gaintable=[tCal], gainfield=['1'])
        polcal(vis=tablecompdata, caltable=calpath2, poltype='Df', gaintable=[tCal])
        
        self.assertFalse(np.all(tableComp(calpath, calpath2)[:,1] == 'True'))
        
        
    def test_interp(self):
        '''
            test_interp
            -------------
            
            Check that this parameter sets the Interpolation mode (in time) to use for each gaintable
        '''
        
        polcal(vis=datapath, caltable=calpath, poltype='Xf', gaintable=[gainpath], interp=['',''])
        polcal(vis=datapath, caltable=calpath2, poltype='Xf', gaintable=[gainpath], interp=['cubic'])
        
        self.assertFalse(np.all(tableComp(calpath, calpath2)[:,1] == 'True'))
        
        
    def test_spwmap(self):
        '''
            test_spwmap
            -------------
            
            Check that the spectral window combinations used to form the gaintable(s) were properly selected
            TODO need a new gaintable for a file that has multiple spectral windows
        '''
        
        polcal(vis=tablecompdata, caltable=calpath, poltype='Df', gaintable=[gCal])
        polcal(vis=tablecompdata, caltable=calpath2, poltype='Df', gaintable=[gCal], spwmap=[0,0,1,1])
        
        self.assertFalse(np.all(tableComp(calpath, calpath2)[:,1] == 'True'))
    
    
def suite(self):
    return[polcal_test]

if __name__ == '__main__':
    unittest.main()
