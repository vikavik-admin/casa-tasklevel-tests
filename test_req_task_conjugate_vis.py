########################################################################
# test_req_task_conjugatevis.py
#
# Copyright (C) 2018
# Associated Universities, Inc. Washington DC, USA
#
# This script is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# [Add the link to the JIRA ticket here once it exists]
#
# Based on the requirements listed in plone found here:
# https://casa.nrao.edu/casadocs-devel/stable/global-task-list/task_conjugatevis/about
#
#
##########################################################################

CASA6 = False
try:
    import casatools
    from casatasks import conjugatevis, casalog, listobs
    CASA6 = True
except ImportError:
    from __main__ import default
    from tasks import *
    from taskinit import *
import sys
import os
import unittest
import shutil
import numpy as np
import pandas as pd

datapath = '/export/home/selene/testing/casa-prerelease-5.6.0-33.el7/casa-data-req/visibilities/vla/ngc5921.ms'
fakepath = '/export/home/selene/testing/casa-prerelease-5.6.0-33.el7/casa-data-req/fits/1904-66_AIR.fits'

logpath = casalog.logfile()

class conjugatevis_test(unittest.TestCase):
    
    def setUp(self):
        if not CASA6:
            default(conjugate_vis)
        else:
            pass
            
    def tearDown(self):
        casalog.setlogfile(logpath)
        if os.path.exists('testlog.log'):
            os.remove('testlog.log')
            
    def test_takesMeasurementSet(self):
        '''1. test_takesMeasurementSet: Check that conjugatevis opens a MeasurementSet file'''
        casalog.setlogfile('testlog.log')
        conjugatevis(vis=datapath)
        self.assertFalse('SEVERE' in open('testlog.log').read())
        
    def test_doesNotTakeOtherFiletypes(self):
        '''2. test_doesNotTakeOtherFiletypes: Check that conjugatevis does not open files other than MS files '''
        casalog.setlogfile('testlog.log')
        conjugatevis(vis=fakepath)
        self.assertTrue('SEVERE' in open('testlog.log').read())
        
    def test_changeSignOneSpectralWindow(self):
        '''3. test_changeSignOnSpectralWindow: Check that conjugatevis changed the phase sign for a single spectral window'''
        conjugatevis(vis=datapath, spwlist = 0, outputvis='ngc5921-conj.ms')
        ''' Save the original file into a text file to check value of phase sign '''
        listobs(vis=datapath, listfile='ngc5921.out')
        ''' Read the text file using pandas '''
        msData = pd.read_csv('ngc5921.out', sep='\t')
        ''' Phase is the 6th column '''
        phase = msData[5]
        ''' Save the conjugated file into a text file '''
        listobs(vis='ngc5921-conj.ms', listfile = 'ngc5921-conj.out')
        msData-conj = pd.read_csv('ngc5921-conj.out', sep='\t')
        phase-conj = msData-conj[5]
        ''' Find the first member of phase which is not equal to zero '''
        nonZeroPhaseIndex = nonzero(phase)[0]
        originalPhase = phase[nonZeroPhaseIndex]
        conjPhase = phase-conj[nonZeroPhaseIndex]
        self.assertTrue(originalPhase == -1*conjPhase)
        
        
        
        
        
        
        