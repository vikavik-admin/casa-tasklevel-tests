##########################################################################
# test_req_task_rerefant.py
#
# Copyright (C) 2018
# Associated Universities, Inc. Washington DC, USA.
#
# This script is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# [Add the link to the JIRA ticket here once it exists]
#
# Based on the requirements listed in plone found here:
# https://casa.nrao.edu/casadocs-devel/stable/global-task-list/task_rerefant/about
#
#
##########################################################################

CASA6 = False
try:
    import casatools
    from casatasks import rerefant, casalog
    CASA6 = True
    tb = casatools.table()
except ImportError:
    from __main__ import default
    from tasks import *
    from taskinit import *
import sys
import os
import unittest
import shutil
import numpy as np

#import pyfits

## DATA ## 

if CASA6:
    casavis = casatools.ctsys.resolve('visibilities/vla/ngc5921.ms/')
    casacal = casatools.ctsys.resolve('caltables/ngc5921.ref1a.gcal')
else:
    if os.path.exists(os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req'):
        casavis = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/vla/ngc5921.ms/'
        casacal = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/caltables/ngc5921.ref1a.gcal'
    else:
        casavis = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/vla/ngc5921.ms/'
        casacal = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/caltables/ngc5921.ref1a.gcal'
        
copyvis = 'vis.ms'
copycal = 'copycal.gcal'

logpath = casalog.logfile()

def file_copy(filename, perm):
    os.chmod(filename, perm)
    for root, dirs, files in os.walk(filename):
        for d in dirs:
            os.chmod(os.path.join(root, d), perm)
        for f in files:
            os.chmod(os.path.join(root, f), perm)
        
class rerefant_test(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        pass
    
    def setUp(self):
        if not CASA6:
            default(rerefant)
        shutil.copytree(casavis, copyvis)
        shutil.copytree(casacal, copycal)
        file_copy(copyvis, 493)
        file_copy(copycal, 493)
    
    def tearDown(self):
        shutil.rmtree(copyvis)
        shutil.rmtree(copycal)
        casalog.setlogfile(logpath)
        
        if os.path.exists('testlog.log'):
            os.remove('testlog.log')
        if os.path.exists('out.cal'):
            shutil.rmtree('out.cal')
    
    @classmethod
    def tearDownClass(cls):
        pass
    
    def test_takesms(self):
        '''
            test_taskesms
            ---------------
            
            Test that the task takes a valid ms.
            
            To operate this task also required a tablein to be given as well but this test only checks valid vs invalid ms
        '''
        
        casalog.setlogfile('testlog.log')
        
        rerefant(vis=copyvis, tablein=copycal, caltable='out.cal', refant='VA01')
        self.assertTrue('SEVERE' not in open('testlog.log').read())
        shutil.rmtree('out.cal')
        
        rerefant(vis=copyvis, tablein=copycal, caltable='out.cal', refant='VA01')
        self.assertTrue('SEVERE' not in open('testlog.log').read())
        
        
    def test_takestablein(self):
        '''
            test_takestablein
            -------------------
            
            Test that the task takes a valid caltable
            
            Check that the invalid inputs will raise a sever error
            
            TODO once the multiple antenna option is fixed test further
        '''
        
        casalog.setlogfile('testlog.log')
        
        rerefant(vis=copyvis, tablein=copycal, caltable='out.cal', refant='VA01')
        self.assertTrue('SEVERE' not in open('testlog.log').read())
        shutil.rmtree('out.cal')
        
        rerefant(vis=copyvis, tablein=copycal, caltable='out.cal', refant='VA01')
        self.assertTrue('SEVERE' not in open('testlog.log').read())
        
    def test_modeflex(self):
        '''
            test_modeflex
            ------------------
            
            Check that the flex parameter doesn't raise any SEVERE errors
            
            Check that it takes multiple antenna (and can switch antennas and back)
            
            TODO Need to be able to check if the antenna drops out and back in but it's not working with multiple antenna right now
        '''
        
        casalog.setlogfile('testlog.log')
        
        rerefant(vis=copyvis, tablein=copycal, caltable='out.cal', refant='VA02', refantmode='flex')
        self.assertTrue('SEVERE' not in open('testlog.log').read())
        
        tb.open('out.cal')
        antennacol = tb.getcol('ANTENNA2')
        self.assertTrue(np.all(antennacol == 1))
        tb.close()
        
        rerefant(vis=copyvis, tablein=copycal, caltable='out.cal', refant='VA01,VA02', refantmode='flex')
        self.assertTrue('SEVERE' not in open('testlog.log').read())
        
        tb.open('out.cal')
        antennacol = tb.getcol('ANTENNA2')
        self.assertTrue(np.all(antennacol == 0))
        tb.close()
        
    def test_modestrict(self):
        '''
            test_modestrict
            -----------------
            
                This mode will flag all antennas if the current refant is absent for a solution
                
                If a list is provided only use the first element of that list
                
                TODO come back to this one (It may be that the muti antenna problem has to be solved before I can test this fully)
        '''
        
        casalog.setlogfile('testlog.log')
        
        rerefant(vis=copyvis, tablein=copycal, caltable='out.cal', refant='VA01', refantmode='strict')
        self.assertTrue('SEVERE' not in open('testlog.log').read())
        
        tb.open('out.cal')
        #print(tb.getcol('FLAG'))
        tb.close()
        
        rerefant(vis=copyvis, tablein=copycal, caltable='out.cal', refant='VA01,VA02', refantmode='strict')
        self.assertTrue('SEVERE' not in open('testlog.log').read())
        
        
        
                
    def test_takescaltable(self):
        '''
            test_takescaltable
            --------------------
            
            Test that the tasks makes an output caltable when ran
        '''
        
        casalog.setlogfile('testlog.log')
        
        rerefant(vis=copyvis, tablein=copycal, caltable='out.cal', refant='VA01')
        self.assertTrue('SEVERE' not in open('testlog.log').read())
        self.assertTrue(os.path.exists('out.cal'))
        
def suite():
    return[rerefant_test]

if __name__ == '__main__':
    unittest.main()
        
