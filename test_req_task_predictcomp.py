##########################################################################
# test_req_task_predictcomp.py
#
# Copyright (C) 2018
# Associated Universities, Inc. Washington DC, USA.
#
# This script is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# [Add the link to the JIRA ticket here once it exists]
#
# Based on the requirements listed in plone found here:
# https://casa.nrao.edu/casadocs-devel/stable/global-task-list/task_predictcomp/about
#
#
##########################################################################

CASA6 = False
try:
    import casatools
    from casatasks import predictcomp
    CASA6 = True
    tb = casatools.table()
except ImportError:
    from __main__ import default
    from tasks import *
    from taskinit import *
import sys
import os
import stat
import unittest
import shutil
import numpy
import casaTestHelper
import pylab
import matplotlib._pylab_helpers

### DATA ###

if CASA6:
    antennapath = casatools.ctsys.resolve('text/alma.cycle1.5.cfg')

else:
    if os.path.exists(os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req'):
        antennapath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/text/alma.cycle1.5.cfg'
        
    else:
        antennapath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/text/alma.cycle1.5.cfg'


test_dict = {}

class predictcomp_test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        pass
        
    def setUp(self):
        if not CASA6:
            default(predictcomp)
        
    def tearDown(self):
        if os.path.exists('Titan_spw0_100.000GHz_58041.0d.cl'):
            shutil.rmtree('Titan_spw0_100.000GHz_58041.0d.cl')
            
        if os.path.exists('TEST_Titan_spw0_100.000GHz_58041.0d.cl'):
            shutil.rmtree('TEST_Titan_spw0_100.000GHz_58041.0d.cl')
            
        if os.path.exists('testimage.png'):
            os.remove('testimage.png')
            
        if os.path.exists('Uranus_spw0_100.000GHz_58270.5d.cl'):
            shutil.rmtree('Uranus_spw0_100.000GHz_58270.5d.cl')
            
        if os.path.exists('Uranus_spw0_100.500GHz_58270.5d.cl'):
            shutil.rmtree('Uranus_spw0_100.500GHz_58270.5d.cl')
            
        if os.path.exists('testdir1'):
            shutil.rmtree('testdir1')
        
        
    @classmethod
    def tearDownClass(cls):
        casaTestHelper.generate_weblog("predictcomp", test_dict, show_passed=True)
        
    @casaTestHelper.stats_dict(test_dict)
    def test_objname(self):
        ''' Check that the correct object name is stored in the python dictionary '''
        
        result = predictcomp('Uranus', standard='Butler-JPL-Horizons 2012', epoch='2018/06/01/12:00', minfreq='100GHz')
        casaTestHelper.add_to_dict(self, output=test_dict, resulting_dict = result)
        
        self.assertTrue('Uranus' == result['objname'])
        
       
    @casaTestHelper.stats_dict(test_dict)
    def test_standard(self):
        ''' Check that the correct standard is stored within the python dictionary '''
        
        result = predictcomp('Uranus', standard='Butler-JPL-Horizons 2012', epoch='2018/06/01/12:00', minfreq='100GHz')
        casaTestHelper.add_to_dict(self, output=test_dict, resulting_dict = result)
        
        self.assertTrue('Butler-JPL-Horizons 2012' == result['standard'])
        
        
    @casaTestHelper.stats_dict(test_dict)
    def test_epoch(self):
        ''' Check that the correct epoch is stored in the python dictionary '''
        
        result = predictcomp('Uranus', standard='Butler-JPL-Horizons 2012', epoch='2018/06/01/12:00', minfreq='100GHz')
        casaTestHelper.add_to_dict(self, output=test_dict, resulting_dict = result)
        
        self.assertTrue(58270.5 == result['epoch']['m0']['value'])
        
        
    @casaTestHelper.stats_dict(test_dict)
    def test_minfreq(self):
        ''' Check that this parameter sets the minimum allowed frequency '''
        
        result = predictcomp('Uranus', standard='Butler-JPL-Horizons 2012', epoch='2018/06/01/12:00', minfreq='100GHz')
        casaTestHelper.add_to_dict(self, output=test_dict, resulting_dict = result)
        
        self.assertTrue(numpy.isclose(100.0, result['freqs (GHz)'][0]))
        
    
    @casaTestHelper.stats_dict(test_dict)
    def test_maxfreq(self):
        ''' Check that this parameter sets the maximum allowed frequency '''
        
        result = predictcomp('Uranus', standard='Butler-JPL-Horizons 2012', epoch='2018/06/01/12:00', minfreq='100GHz', maxfreq='120GHz')
        casaTestHelper.add_to_dict(self, output=test_dict, resulting_dict = result)
        
        self.assertTrue(numpy.isclose(120.0, result['freqs (GHz)'][1]))
        
        
    @casaTestHelper.stats_dict(test_dict)
    def test_nfreqs(self):
        ''' Check that this parmeter sets the frequency interval for the predicted visibilities '''
        
        result = predictcomp(objname="Titan", standard="Butler-JPL-Horizons 2012", epoch="2017/10/15/00:00", minfreq="100GHz", maxfreq="120GHz", nfreqs=5)
        casaTestHelper.add_to_dict(self, output=test_dict, resulting_dict = result)
        
        self.assertTrue(5 == len(result['freqs (GHz)']))
        
    
    @casaTestHelper.stats_dict(test_dict)
    def test_prefix(self):
        ''' Check that the prefix parameter adds a prefix to the output cl file '''
        result = predictcomp(objname="Titan", standard="Butler-JPL-Horizons 2012", epoch="2017/10/15/00:00", minfreq="100GHz", maxfreq="120GHz", nfreqs=5, prefix='TEST_')
        casaTestHelper.add_to_dict(self, output=test_dict, resulting_dict = result)
        
        self.assertTrue(os.path.exists('TEST_Titan_spw0_100.000GHz_58041.0d.cl'))
        
        
    @casaTestHelper.stats_dict(test_dict)
    def test_antennalist(self):
        ''' Check that the task predicts the visibility amplitudes '''
        result = predictcomp(objname="Titan", standard="Butler-JPL-Horizons 2012", epoch="2017/10/15/00:00", minfreq="100GHz", maxfreq="120GHz", nfreqs=5, antennalist=antennapath)
        casaTestHelper.add_to_dict(self, output=test_dict, resulting_dict = result)
        
        self.assertTrue('amps' in result.keys())
        
    @casaTestHelper.stats_dict(test_dict)
    def test_showplot(self):
        ''' Check that the plot is displayed '''
        result = predictcomp(objname="Titan", standard="Butler-JPL-Horizons 2012", epoch="2017/10/15/00:00", minfreq="100GHz", maxfreq="120GHz", nfreqs=5, antennalist=antennapath)
        casaTestHelper.add_to_dict(self, output=test_dict, resulting_dict = result)
        
        self.assertTrue('amps' in result.keys())
        
        
        
    @casaTestHelper.stats_dict(test_dict)
    def test_savefig(self):
        ''' Check that the figure is saved according to the provided name '''
        result = predictcomp(objname="Titan", standard="Butler-JPL-Horizons 2012", epoch="2017/10/15/00:00", minfreq="100GHz", maxfreq="120GHz", nfreqs=5, antennalist=antennapath, savefig='testimage.png')
        casaTestHelper.add_to_dict(self, output=test_dict, resulting_dict = result)
        
        num_plots = len(matplotlib._pylab_helpers.Gcf.get_all_fig_managers())
        
        self.assertTrue(num_plots > 0)
        
    @casaTestHelper.stats_dict(test_dict)
    def test_needObject(self):
        ''' Check that an error is raised if no object is provided '''
        casaTestHelper.add_to_dict(self, output=test_dict)
        if CASA6:
            with self.assertRaises(Exception):
                predictcomp(objname='' , standard='Butler-JPL-Horizons 2012', epoch='2018/06/01/12:00', minfreq='100GHz')
        else:
            result = predictcomp(objname='' , standard='Butler-JPL-Horizons 2012', epoch='2018/06/01/12:00', minfreq='100GHz')
            self.assertTrue(type(result) == type(None))
            
    @casaTestHelper.stats_dict(test_dict)
    def test_needMinfreq(self):
        ''' Check that an error is raised if no minfreq is provided '''
        casaTestHelper.add_to_dict(self, output=test_dict)
        if CASA6:
            with self.assertRaises(Exception):
                predictcomp(objname='Uranus' , standard='Butler-JPL-Horizons 2012', epoch='2018/06/01/12:00', minfreq='')
        else:
            result = predictcomp(objname='Uranus' , standard='Butler-JPL-Horizons 2012', epoch='2018/06/01/12:00', minfreq='')
            self.assertTrue(type(result) == type(None))
            
    @casaTestHelper.stats_dict(test_dict)
    def test_unitsMaxfreq(self):
        ''' Check that if valid maxfreq units are given only the minfreq is used '''
        result = predictcomp(objname='Uranus' , standard='Butler-JPL-Horizons 2012', epoch='2018/06/01/12:00', minfreq='100GHz', maxfreq='404Fake')
        casaTestHelper.add_to_dict(self, output=test_dict, resulting_dict=result)
        
        self.assertTrue(len(result['freqs (GHz)']) == 1)
        
    @casaTestHelper.stats_dict(test_dict)
    def test_nfreqsLimit(self):
        ''' Check that when minfreq != maxfreq and nfreq < 2 then two frequencies will still be used '''
        result = predictcomp(objname='Uranus' , standard='Butler-JPL-Horizons 2012', epoch='2018/06/01/12:00', minfreq='100GHz', maxfreq='120GHz', nfreqs=1)
        casaTestHelper.add_to_dict(self, output=test_dict, resulting_dict=result)
        
        self.assertTrue(len(result['freqs (GHz)']) == 2)
        
    @casaTestHelper.stats_dict(test_dict)
    def test_dirPrefix(self):
        ''' Check that you can make the prefix multiple directories '''
        result = predictcomp(objname='Uranus' , standard='Butler-JPL-Horizons 2012', epoch='2018/06/01/12:00', minfreq='100GHz', prefix='testdir1/testdir2/')
        casaTestHelper.add_to_dict(self, output=test_dict, resulting_dict=result)
        
        self.assertTrue(os.path.exists('testdir1'))
        
    @casaTestHelper.stats_dict(test_dict)
    def test_noWrite(self):
        ''' Check that no dictionary get made and no file is written '''
        result = predictcomp(objname='Uranus' , standard='Butler-JPL-Horizons 2012', epoch='2018/06/01/12:00', minfreq='100GHz', prefix='/testdir1/')
        casaTestHelper.add_to_dict(self, output=test_dict, resulting_dict=result)
        
        self.assertFalse(result)
        
    @casaTestHelper.stats_dict(test_dict)
    def test_floatmin(self):
        ''' Check that decimal values are handled properly '''
        result = predictcomp('Uranus', standard='Butler-JPL-Horizons 2012', epoch='2018/06/01/12:00', minfreq='100.5GHz')
        casaTestHelper.add_to_dict(self, output=test_dict, resulting_dict = result)
        
        self.assertTrue(numpy.isclose(100.5, result['freqs (GHz)'][0]))
        
    @casaTestHelper.stats_dict(test_dict)
    def test_removeDuplicate(self):
        ''' Check that if the task is run twice the existing file will be overwritten '''
        result1 = predictcomp('Uranus', standard='Butler-JPL-Horizons 2012', epoch='2018/06/01/12:00', minfreq='100GHz')
        first_made = os.path.getmtime('Uranus_spw0_100.000GHz_58270.5d.cl/')
        result2 = predictcomp('Uranus', standard='Butler-JPL-Horizons 2012', epoch='2018/06/01/12:00', minfreq='100GHz')
        second_made = os.path.getmtime('Uranus_spw0_100.000GHz_58270.5d.cl/')
        casaTestHelper.add_to_dict(self, output=test_dict)
        
        self.assertTrue(first_made != second_made)
    
    @casaTestHelper.stats_dict(test_dict)
    def test_blunit(self):
        ''' Check that the blunit is changed to the value of this parameter '''
        result = predictcomp(objname="Titan", standard="Butler-JPL-Horizons 2012", epoch="2017/10/15/00:00", minfreq="100GHz", maxfreq="120GHz", nfreqs=5, antennalist=antennapath, blunit='klambda')
        casaTestHelper.add_to_dict(self, output=test_dict, resulting_dict = result)
        
        self.assertTrue(result['blunit'] == 'k$\\lambda$')
        
        
def suite():
    return[predictcomp_test]

if __name__ == "__main__":
    unittest.main()
