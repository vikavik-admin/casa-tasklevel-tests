##########################################################################
# test_req_task_smoothcal.py
#
# Copyright (C) 2018
# Associated Universities, Inc. Washington DC, USA.
#
# This script is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# [Add the link to the JIRA ticket here once it exists]
#
# Based on the requirements listed in plone found here:
# https://casa.nrao.edu/casadocs-devel/stable/global-task-list/task_smoothcal/about
#
#
##########################################################################

CASA6 = False
try:
    import casatools
    from casatasks import smoothcal
    CASA6 = True
    tb = casatools.table()
except ImportError:
    from __main__ import default
    from tasks import *
    from taskinit import *
import sys
import os
import unittest
import shutil
#import casaTestHelper
import numpy

### DATA ###

if CASA6:
    datapath = casatools.ctsys.resolve('visibilities/vla/gaincaltest2.ms/')
    calpath = casatiiks.ctsys.resolve('caltables/gaincaltest2.ms.T0')

else:
    if os.path.exists(os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req'):
        datapath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/vla/gaincaltest2.ms/'
        calpath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/caltables/gaincaltest2.ms.T0'

    else:
        datapath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/vla/gaincaltest2.ms/'
        calpath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/caltables/gaincaltest2.ms.T0'

calcopy = 'calcopy.cal'
calout = 'smoothcalTest.cal'

def getmean(data):
    tb.open(data)
    datamean = numpy.mean(tb.getcol('CPARAM'))
    tb.close()
    return datamean

class smoothcal_test(unittest.TestCase):

    def setUp(self):
        shutil.copytree(calpath, calcopy)

    def tearDown(self):
        shutil.rmtree(calcopy)
    
        if os.path.exists(calout):
            shutil.rmtree(calout)
    
    @classmethod
    def tearDownClass(cls):
        # Write to the casatestHelper weblog here
        pass

    def test_flagged(self):
        pass

    def test_smoothApplied(self):
        ''' Test that the smoothing as applied to the output calibration table '''
        smoothcal(vis=datapath, tablein=calcopy, caltable=calout)
        datamean = getmean(calout)
    
        self.assertTrue(numpy.isclose(datamean, 0.89653042011247408-0.010022666993861696j))

    def test_vis(self):
        ''' Check that the vis parameter causes a failure if missing '''
        smoothcal(tablein=calcopy, caltable=calout)
    
        self.assertFalse(os.path.exists(calout))

    def test_inputTable(self):
        ''' Check that an input table is needed for the task to run '''
        smoothcal(vis=datapath, caltable=calout)
    
        self.assertFalse(os.path.exists(calout))

    def test_outputTable(self):
        ''' Check that a table is produced with the name provided by the calout parameter '''
        smoothcal(vis=datapath, tablein=calcopy, caltable=calout)
    
        self.assertTrue(os.path.exists(calout))

    def test_inputTableOverwrite(self):
        ''' Check that if no caltable is given the input table is overwritten '''
        smoothcal(vis=datapath, tablein=calcopy)
        datamean = getmean(calcopy)
    
        self.assertTrue(numpy.isclose(datamean, 0.89653042011247408-0.010022666993861696j))

    def test_fieldSelect(self):
        ''' Check that the field selection will only smooth that field '''
        smoothcal(vis=datapath, tablein=calcopy, caltable=calout, field=1)
        datamean = getmean(calout)
    
        self.assertTrue(numpy.isclose(datamean, 0.89653116559321633-0.010003578392770652j))

    def test_smoothType(self):
        ''' Check that the smoothtype parameter changes the output caltable '''
        smoothcal(vis=datapath, tablein=calcopy, caltable=calout, smoothtype='mean')
        datamean = getmean(calout)
    
        self.assertTrue(numpy.isclose(datamean, 0.89657961919744911-0.0099825490960341514j))

    def test_timeScale(self):
        ''' Check that the smoothtime parameter changes the smoothing timescale '''
        smoothcal(vis=datapath, tablein=calcopy, caltable=calout, smoothtime=760)
        datamean = getmean(calout)
                        
        self.assertTrue(numpy.isclose(datamean, 0.89677176214733556-0.0099920187741845221j))


def suite():
    return[smoothcal_test]

if __name__ == "__main__":
    unittest.main()
