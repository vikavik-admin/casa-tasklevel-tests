##########################################################################
# test_req_task_setjy.py
#
# Copyright (C) 2018
# Associated Universities, Inc. Washington DC, USA.
#
# This script is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# [Add the link to the JIRA ticket here once it exists]
#
# Based on the requirements listed in plone found here:
# https://casa.nrao.edu/casadocs/casa-5.4.0/global-task-list/task_setjy/about
#
# test import: Checks that importing works and doesn't accept fake inputs
# test flagfile: Checks the flagfile param and that fake flag files cause failure
#
##########################################################################

CASA6 = False
try:
    import casatools
    from casatasks import setjy
    tb = casatools.table()
    CASA6 = True
except ImportError:
    from __main__ import default
    from tasks import *
    from taskinit import *
import sys
import os
import unittest
import shutil
import casaTestHelper
import numpy

# DATA #
if CASA6:
    datapath = casatools.ctsys.resolve('visibilities/alma/twhya_setjy.ms')
    dataForModel = casatools.ctsys.resolve('visibilities/vla/ngc5921.ms')
    butler2012data = casatools.ctsys.resolve('visibilities/alma/nep2-shrunk.ms/')
else:
    if os.path.exists(os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/'):
        datapath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/alma/twhya_setjy.ms'
        dataForModel = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/vla/ngc5921.ms'
        butler2012data = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/alma/nep2-shrunk.ms/'
        
    else:
        datapath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/alma/twhya_setjy.ms'
        dataForModel = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/vla/ngc5921.ms'
        butler2012data = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/alma/nep2-shrunk.ms/'
    
datacopy = 'twhya_setjy_copy.ms'
test_dict = {}

def get_colnames(data):
    tb.open(data)
    result = tb.colnames()
    tb.close()
    return result
    
class setjy_test(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        pass
    
    def setUp(self):
        shutil.copytree(datapath, datacopy)
        
        if not CASA6:
            default(setjy)
    
    def tearDown(self):
        if os.path.exists(datacopy):
            shutil.rmtree(datacopy)
    
    @classmethod
    def tearDownClass(cls):
        casaTestHelper.generate_weblog("setjy", test_dict)
    
    @casaTestHelper.stats_dict(test_dict)
    def test_virtualModel(self):
        ''' Check that a virtual model is made if usescratch is not set to True '''
        setjy(datacopy, standard='Butler-JPL-Horizons 2010', field='Titan')
        casaTestHelper.add_to_dict(self, output=test_dict, dataset=datacopy)
        column_names = get_colnames(datacopy+'/SOURCE')
        
        self.assertTrue('SOURCE_MODEL' in column_names)
        
    @casaTestHelper.stats_dict(test_dict)
    def test_scratchModel(self):
        ''' Check that a scratch model column is added if usescratch is set to True '''
        setjy(datacopy, standard='Butler-JPL-Horizons 2010', field='Titan', usescratch=True)
        casaTestHelper.add_to_dict(self, output=test_dict, dataset=datacopy)
        column_names = get_colnames(datacopy)
        
        self.assertTrue('MODEL_DATA' in column_names)
        
    @casaTestHelper.stats_dict(test_dict)
    def test_standardManual(self):
        ''' The flux density of a point-source calibrator can be set explicitly '''
        result = setjy(datacopy, standard='manual', field='3c279')
        casaTestHelper.add_to_dict(self, output=test_dict, dataset=datacopy, result=result)
        
        self.assertTrue(len(result) > 0)
        
    @casaTestHelper.stats_dict(test_dict)
    def test_noModel(self):
        ''' If no model is given then assume a point source at the phase center '''
        
        #TODO how do I determine if this is a phase center
        setjy(datacopy, field='3c279', usescratch=True)
        casaTestHelper.add_to_dict(self, output=test_dict, dataset=datacopy)
        tb.open(datacopy)
        data = tb.getcol('MODEL_DATA')
        tb.close()
        
        self.assertTrue(numpy.all(1==data))
        
    @casaTestHelper.stats_dict(test_dict)
    def test_spwSelect(self):
        ''' Check that the spw selects the data properly '''
        
        res = setjy(datacopy, spw='0')
        casaTestHelper.add_to_dict(self, output=test_dict, dataset=datacopy, result=res)
        spws = []
        for i in res.keys():
            if type(res[i]) == type({}):
                spws.append(list(res[i].keys())[0] == '0')
                
        self.assertTrue(spws)
        
    @casaTestHelper.stats_dict(test_dict)
    def test_manualFluxDensity(self):
        ''' Dictionary contains specified specified fluxdensity '''
        
        res = setjy(datacopy, field='3c279', standard='manual', fluxdensity=[2,0,0,0])
        casaTestHelper.add_to_dict(self, output=test_dict, dataset=datacopy, result=res)
        densities = []
        for item in res['0']:
            try:
                densities.append(res['0'][item]['fluxd'] == [2,0,0,0])
            except:
                pass
            
        self.assertTrue(numpy.all(densities))
        
    @casaTestHelper.stats_dict(test_dict)
    def test_manualSpectralIndex(self):
        ''' Check that the spectral index can be set manually '''
        
        res = setjy(datacopy, field='3c279', standard='manual', spix=[2], fluxdensity=[1,0,0,0])
        casaTestHelper.add_to_dict(self, output=test_dict, dataset=datacopy, result=res)
        print(res)
        self.assertTrue(numpy.isclose(res['0']['0']['fluxd'][0], 127095.02166498))
    
    @casaTestHelper.stats_dict(test_dict)
    def test_manualRefFeq(self):
        ''' Check that the reference frequency for spix can be set manually '''
        
        res = setjy(datacopy, field='3c279', standard='manual', spix=[1], reffreq='2GHz', fluxdensity=[1,0,0,0])
        casaTestHelper.add_to_dict(self, output=test_dict, dataset=datacopy, result=res)
        print(res)
        self.assertTrue(numpy.isclose(res['0']['0']['fluxd'][0], 178.25194365))
        
    @casaTestHelper.stats_dict(test_dict)
    def test_manualPolIndexAngle(self):
        ''' Check that the pol index and angle can be set manually '''
        
        # Needs the use of polangle to work properly
        # Using these alone seems to do nothing
        res = setjy(datacopy, field='3c279', standard='manual', polindex=[.2], polangle=[-1.7], fluxdensity=[1,0,0,0])
        casaTestHelper.add_to_dict(self, output=test_dict, dataset=datacopy, result=res)
        expected = [1, -0.19335964, 0.05110822, 0]
        
        self.assertTrue(numpy.all(numpy.isclose(res['0']['0']['fluxd'], expected)))
        
    @casaTestHelper.stats_dict(test_dict)
    def test_fieldSelect(self):
        ''' Check the field selection parameter properly selects a field '''
        
        res = setjy(datacopy, field='TW Hya')
        casaTestHelper.add_to_dict(self, output=test_dict, dataset=datacopy, result=res)
        
        self.assertTrue(res['2']['fieldName'] == 'TW Hya')
        
    @casaTestHelper.stats_dict(test_dict)
    def test_spwSelect(self):
        ''' Check that the spw selection parameter properly selects spectral windows '''
        
        res = setjy(datacopy, field='TW Hya', spw='1')
        casaTestHelper.add_to_dict(self, output=test_dict, dataset=datacopy, result=res)
        expected = ['1','fieldName']
        
        self.assertTrue(list(res['2'].keys()) == expected)
        
    @casaTestHelper.stats_dict(test_dict)
    def test_manualRotmeas(self):
        ''' Check that the rotmeas parameter for standard = 'manual' sets the rotation measure in rad/m^2 '''
        # This requires the polindex and polangle parameters to have any effect
        
        res = setjy(datacopy, field='3c279', standard='manual', polindex=[.2], polangle=[-1.7], rotmeas=0.6, fluxdensity=[1,0,0,0])
        casaTestHelper.add_to_dict(self, output=test_dict, dataset=datacopy, result=res)
        expected = [1.0, -0.18673486, 0.07162466, 0.0]
        
        self.assertTrue(numpy.all(numpy.isclose(res['0']['0']['fluxd'], expected)))
        
    @casaTestHelper.stats_dict(test_dict)
    def test_listmodels(self):
        ''' Check that the listmodels param causes the task to list the Canadate modimages '''
        
        res = setjy(datacopy, field='3c279', listmodels=True)
        casaTestHelper.add_to_dict(self, output=test_dict, dataset=datacopy, result=res)
        
        self.assertTrue(res == True)
        
    @casaTestHelper.stats_dict(test_dict)
    def test_useModel(self):
        ''' Check that the Model provided is used by setjy '''
        
        res = setjy(dataForModel, field='1331+30500002_0', model='3C286_K.im')
        casaTestHelper.add_to_dict(self, output=test_dict, dataset=dataForModel, result=res)
        
        self.assertTrue(numpy.isclose(res['0']['0']['fluxd'][0], 15.01586437))
        
    @casaTestHelper.stats_dict(test_dict)
    def test_standardDiff(self):
        ''' Check that changing the standard parameter to a proper standard changes the output '''
        
        res = setjy(datacopy, field='Titan')
        flux1 = res['1']['0']['fluxd']
        print(flux1)
        res = setjy(datacopy, field='Titan', standard='Butler-JPL-Horizons 2010')
        flux2 = res['1']['0']['fluxd']
        casaTestHelper.add_to_dict(self, output=test_dict, dataset=datacopy, result=[flux1, flux2])
        
        self.assertFalse(numpy.all(numpy.isclose(flux1, flux2)))
        
    @casaTestHelper.stats_dict(test_dict)
    def test_standardFluxscale(self):
        ''' Check that the fluxscale mode for standard works as expected '''
        
        flux_res = {'spwID': numpy.array([0, 1, 2, 3]), 'spwName': numpy.array(['', '', '', '']), '0': {'fitRefFreq': 350998124124.99243, 'spidxerr': numpy.array([  9.08346853e-09,   1.12781134e-06]), 'fitFluxd': 1.0000000094780979, 'spidx': numpy.array([  4.11628558e-09,  -2.40507782e-06]), '1': {'fluxdErr': numpy.array([ 0.0155005,  0.       ,  0.       ,  0.       ]), 'numSol': numpy.array([ 16.,   0.,   0.,   0.]), 'fluxd': numpy.array([ 0.99999995,  0.        ,  0.        ,  0.        ])}, '0': {'fluxdErr': numpy.array([ 0.01445721,  0.        ,  0.        ,  0.        ]), 'numSol': numpy.array([ 16.,   0.,   0.,   0.]), 'fluxd': numpy.array([ 0.99999999,  0.        ,  0.        ,  0.        ])}, '3': {'fluxdErr': numpy.array([ 0.01271945,  0.        ,  0.        ,  0.        ]), 'numSol': numpy.array([ 16.,   0.,   0.,   0.]), 'fluxd': numpy.array([ 1.00000008,  0.        ,  0.        ,  0.        ])}, '2': {'fluxdErr': numpy.array([ 0.01797085,  0.        ,  0.        ,  0.        ]), 'numSol': numpy.array([ 14.,   0.,   0.,   0.]), 'fluxd': numpy.array([ 0.99999999,  0.        ,  0.        ,  0.        ])}, 'fieldName': '3c279', 'covarMat': numpy.array([[  5.50007823e-05,   3.29576549e-04],
       [  3.29576549e-04,   8.47887721e-01]]), 'fitFluxdErr': 2.091545942557039e-08}, '3': {'fitRefFreq': 350998124124.99243, 'spidxerr': numpy.array([  8.79233288e-09,   1.15729731e-06]), 'fitFluxd': 1.0000000374440368, 'spidx': numpy.array([  1.62617382e-08,   1.04590670e-06]), '1': {'fluxdErr': numpy.array([ 0.02782958,  0.        ,  0.        ,  0.        ]), 'numSol': numpy.array([ 16.,   0.,   0.,   0.]), 'fluxd': numpy.array([ 1.00000011,  0.        ,  0.        ,  0.        ])}, '0': {'fluxdErr': numpy.array([ 0.01986162,  0.        ,  0.        ,  0.        ]), 'numSol': numpy.array([ 16.,   0.,   0.,   0.]), 'fluxd': numpy.array([ 1.00000003,  0.        ,  0.        ,  0.        ])}, '3': {'fluxdErr': numpy.array([ 0.02602127,  0.        ,  0.        ,  0.        ]), 'numSol': numpy.array([ 16.,   0.,   0.,   0.]), 'fluxd': numpy.array([ 1.00000004,  0.        ,  0.        ,  0.        ])}, '2': {'fluxdErr': numpy.array([ 0.02093823,  0.        ,  0.        ,  0.        ]), 'numSol': numpy.array([ 14.,   0.,   0.,   0.]), 'fluxd': numpy.array([ 1.,  0.,  0.,  0.])}, 'fieldName': 'J1147-382=QSO', 'covarMat': numpy.array([[  1.31873628e-04,  -1.95847206e-04],
       [ -1.95847206e-04,   2.28475479e+00]]), 'fitFluxdErr': 2.0245095376208606e-08}, '2': {'fitRefFreq': 350998124124.99243, 'spidxerr': numpy.array([  2.78461820e-08,   3.56542890e-06]), 'fitFluxd': 0.9999999797215974, 'spidx': numpy.array([ -8.80679847e-09,  -3.36889062e-06]), '1': {'fluxdErr': numpy.array([ 0.06236706,  0.        ,  0.        ,  0.        ]), 'numSol': numpy.array([ 16.,   0.,   0.,   0.]), 'fluxd': numpy.array([ 0.99999982,  0.        ,  0.        ,  0.        ])}, '0': {'fluxdErr': numpy.array([ 0.06790407,  0.        ,  0.        ,  0.        ]), 'numSol': numpy.array([ 16.,   0.,   0.,   0.]), 'fluxd': numpy.array([ 1.00000005,  0.        ,  0.        ,  0.        ])}, '3': {'fluxdErr': numpy.array([ 0.08608958,  0.        ,  0.        ,  0.        ]), 'numSol': numpy.array([ 16.,   0.,   0.,   0.]), 'fluxd': numpy.array([ 0.99999999,  0.        ,  0.        ,  0.        ])}, '2': {'fluxdErr': numpy.array([ 0.07348894,  0.        ,  0.        ,  0.        ]), 'numSol': numpy.array([ 14.,   0.,   0.,   0.]), 'fluxd': numpy.array([ 1.00000007,  0.        ,  0.        ,  0.        ])}, 'fieldName': 'TW Hya', 'covarMat': numpy.array([[  1.31672477e-03,  -3.51914740e-02],
       [ -3.51914740e-02,   2.15867496e+01]]), 'fitFluxdErr': 6.411820223368368e-08}, 'freq': numpy.array([  3.56732250e+11,   3.57968628e+11,   3.45800000e+11,
         3.43721622e+11])}
       
        res = setjy(datacopy, field='TW Hya', standard='fluxscale', fluxdict=flux_res)
        casaTestHelper.add_to_dict(self, output=test_dict, dataset=datacopy, result=res)
        expected = {'2': {'1': {'fluxd': ([ 0.99999992,  0.,  0.,  0.])}, '0': {'fluxd': ([ 0.99999993,  0.,  0.,  0.])}, '3': {'fluxd': ([ 1.00000005,  0.,  0.,  0.])}, '2': {'fluxd': ([ 1.00000003,  0.,  0.,  0.])}, 'fieldName': 'TW Hya'}, 'format': "{field Id: {spw Id: {fluxd: [I,Q,U,V] in Jy}, 'fieldName':field name }}"}
       
        assertionCheck = []
        for i in res['2']:
            if i != 'fieldName':
                assertionCheck.append(numpy.all(numpy.isclose(res['2'][i]['fluxd'], expected['2'][i]['fluxd'])))
        
        self.assertTrue(numpy.all(assertionCheck))
        
    @casaTestHelper.stats_dict(test_dict)
    def test_scaleByChan(self):
        ''' Check that the scalebychan parameter will provide one solution per spectral window '''
        
        res = setjy(datacopy, field='Titan', standard='Butler-JPL-Horizons 2010', scalebychan=False)
        casaTestHelper.add_to_dict(self, output=test_dict, dataset=datacopy, result=res)
        expected = [3.3137510, 3.335420424, 3.1248338, 3.0894672]
        output = []
        for i in range(4):
            output.append(res['1'][str(i)]['fluxd'][0])
            
        print(output, expected)
        self.assertTrue(numpy.all(numpy.isclose(output,expected)))
        
    @casaTestHelper.stats_dict(test_dict)
    def test_dictReturn(self):
        ''' Check that the setjy task returns a dictionary containing flux densities '''
        
        res = setjy(datacopy, field='Titan', standard='Butler-JPL-Horizons 2010')
        casaTestHelper.add_to_dict(self, output=test_dict, dataset=datacopy, result=res)
        
        self.assertTrue(type(res) == type({}))
        
    @casaTestHelper.stats_dict(test_dict)
    def test_standardButler2012models(self):
        ''' Test the standard mode Butler JPL Horizons 2012 with listmodels prints the availiable models'''
        
        res = setjy(butler2012data, field='Neptune', standard='Butler-JPL-Horizons 2012', listmodels=True)
        casaTestHelper.add_to_dict(self, output=test_dict, dataset=butler2012data, result=res)
        
        self.assertTrue(res)
        
    @casaTestHelper.stats_dict(test_dict)
    def test_standardButler2010models(self):
        ''' Test the standard mode Butler JPL Horizons 2010 with listmodels prints the availiable models '''
        
        res = setjy(datacopy, field='Titan', standard='Butler-JPL-Horizons 2010', listmodels=True)
        casaTestHelper.add_to_dict(self, output=test_dict, dataset=datacopy, result=res)
        
        self.assertTrue(res)
        
    @casaTestHelper.stats_dict(test_dict)
    def test_invalidMs(self):
        ''' Check that the task does not except and invalid ms '''
        
        # This probably wont work in CASA 5 (It still adds to the test coverage)
        try:
            res = setjy('fake.ms')
            self.fail()
        except RuntimeError:
            casaTestHelper.add_to_dict(self, output=test_dict, dataset='fake.ms')
            pass
            #self.assertTrue(type(res) == type(None))
        
    
def suite():
    return[setjy_test]

if __name__ == '__main__':
    unittest.main()
