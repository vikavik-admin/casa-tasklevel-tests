########################################################################
# test_req_task_clearstat.py
#
# Copyright (C) 2018
# Associated Universities, Inc. Washington DC, USA
#
# This script is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# [Add the link to the JIRA ticket here once it exists]
#
# Based on the requirements listed in plone found here:
# https://casa.nrao.edu/casadocs-devel/stable/global-task-list/task_clearstat/about
#
#
##########################################################################

CASA6 = False
try:
    import casatools
    from casatasks import clearstat, casalog
    CASA6 = True
    tb = casatools.table()
except ImportError:
    from __main__ import default
    from tasks import *
    from taskinit import *
import sys
import os
import unittest

if CASA6:
    casaimagepath = casatools.ctsys.resolve('image/ngc5921.clean.image')
else:
    if os.path.exists(os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req'):
        casaimagepath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/image/ngc5921.clean.image'
    else:
        casaimagepath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/image/ngc5921.clean.image'

logpath = casalog.logfile()

class clearstat_test(unittest.TestCase):
    
    def setUp(self):
        if not CASA6:
            default(clearstat)

    def tearDown(self):
        casalog.setlogfile(logpath)
        if os.path.exists('testlog.log'):
            os.remove('testlog.log')

    def test_clearsLock(self):
        '''1. test_clearsLock: Check that clearstat clears an autolock'''
        casalog.setlogfile('testlog.log')
        testTable = tb.open(casaimagepath)
        tb.lock()
        isLocked = tb.haslock()
        clearstat()
        isUnlocked = tb.haslock()
        self.assertFalse(isUnlocked)

def suite():
    return[clearstat_test]
        
# Main #
if __name__ == '__main__':
    unittest.main()

