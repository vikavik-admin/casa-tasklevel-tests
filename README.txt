This is a repository for task level tests based on the Plone documentation.
More information will be added to this readme as a time goes on.

Information on each of the test
------------------------------------

Test_name           | path/data                                             | data_size | date_created | telescope | data_type | desc

test_req_task_accor   visibilities/alma/uid___X02_X3d737_X1_01_small.ms/        36 M      21-Feb-2010    ALMA        MS
                      visibilities/alma/Itziar.ms/                              29 M      Simulated      ALMA        MS
                      text/testcallib.txt                                       
                      visibilities/vla/ngc5921.ms/                              36 M      
