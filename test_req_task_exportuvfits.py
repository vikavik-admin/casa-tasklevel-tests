##########################################################################
# test_req_task_exportuvfits.py
#
# Copyright (C) 2018
# Associated Universities, Inc. Washington DC, USA.
#
# This script is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# [Add the link to the JIRA ticket here once it exists]
#
# Based on the requirements listed in plone found here:
# https://casa.nrao.edu/casadocs-devel/stable/global-task-list/task_exportuvfits/about
#
#
##########################################################################

CASA6 = False
try:
    import casatools
    from casatasks import exportuvfits, casalog, flagdata, split
    CASA6 = True
    tb = casatools.table()
except ImportError:
    from __main__ import default
    from tasks import *
    from taskinit import *
import sys
import os
import unittest
import shutil
import filecmp
#from astropy.io import fits

## DATA ## 

if CASA6:
    from astropy.io import fits
    casavis = casatools.ctsys.resolve('visibilities/vla/ngc5921.ms/')
    modelvis = casatools.ctsys.resolve('visibilities/vla/flagdatatest.ms')
    othervis = casatools.ctsys.resolve('visibilities/other/split1scan.ms/')
else:
    import pyfits as fits
    if os.path.exists(os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req'):
        casavis = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/vla/ngc5921.ms/'
        othervis = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/other/split1scan.ms/'
        modelvis = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/vla/flagdatatest.ms'
    else:
        casavis = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/vla/ngc5921.ms/'
        othervis = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/other/split1scan.ms/'
        modelvis = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/vla/flagdatatest.ms'
        
copyvis = 'copy.ms'
        
logpath = casalog.logfile()

class exportuvfits_test(unittest.TestCase):
    
    @classmethod
    def setUpClass(self):
        pass
    
    def setUp(self):
        if not CASA6:
            default(exportuvfits)
    
    def tearDown(self):
        casalog.setlogfile(logpath)
        if os.path.exists('testlog.log'):
            os.remove('testlog.log')
        if os.path.exists('test.UVFITS'):
            os.remove('test.UVFITS')
        if os.path.exists('test2.UVFITS'):
            os.remove('test2.UVFITS')
    
    @classmethod
    def tearDownClass(self):
        if os.path.exists('temp.ms'):
            shutil.rmtree('temp.ms')
        if os.path.exists(copyvis):
            shutil.rmtree(copyvis)
        if os.path.exists('copy.ms.flagversions'):
            shutil.rmtree('copy.ms.flagversions')
    
    
    def test_takesms(self):
        '''
            test_takesms
            --------------
            
            Check that the task takes a valid ms
        '''
        
        self.assertTrue(exportuvfits(casavis, fitsfile='test.UVFITS'))
        
    def test_exportsuvfits(self):
        '''
            test_exportsuvfits
            --------------------
            
            Test that the task makes a uvfits file named after the fitsfile parameter
        '''
        
        exportuvfits(casavis, fitsfile='test.UVFITS')
        self.assertTrue(os.path.exists('test.UVFITS'))
        
    def test_spwselect(self):
        '''
            test_spwselect
            ----------------
            
            Check that the spw selection specifies the spectral windows to use in the UVFITS file
            
            Also check that an invalid selection will not be allowed
        '''
        
        exportuvfits(othervis, fitsfile='test.UVFITS', spw='')
        noselectsize = os.path.getsize('test.UVFITS')
        os.remove('test.UVFITS')
        
        exportuvfits(othervis, fitsfile='test.UVFITS', spw='1')
        self.assertTrue(os.path.exists('test.UVFITS'))
        selectsize = os.path.getsize('test.UVFITS')
        os.remove('test.UVFITS')
        
        self.assertTrue(noselectsize > selectsize)
        
        if CASA6:
            with self.assertRaises(RuntimeError):
                exportuvfits(casavis, fitsfile='test.UVFITS', spw='1')
        else:
            casalog.setlogfile('testlog.log')
            exportuvfits(casavis, fitsfile='test.UVFITS', spw='1')
            self.assertTrue('SEVERE' in open('testlog.log').read())
        
    def test_fieldselect(self):
        '''
            test_fieldselect
            ------------------
            
            Check that the field selection specifies the fields to use in the UVFITS file
            
            Also check that an invalid field id will not be allowed
        '''
        
        exportuvfits(casavis, fitsfile='test.UVFITS', field='')
        noselectsize = os.path.getsize('test.UVFITS')
        os.remove('test.UVFITS')
        
        exportuvfits(casavis, fitsfile='test.UVFITS', field='0~1')
        selectsize = os.path.getsize('test.UVFITS')
        self.assertTrue(os.path.exists('test.UVFITS'))
        os.remove('test.UVFITS')
        
        self.assertTrue(noselectsize > selectsize)
        
        if CASA6:
            with self.assertRaises(RuntimeError):
                exportuvfits(casavis, fitsfile='test.UVFITS', field='3')
        else:
            casalog.setlogfile('testlog.log')
            exportuvfits(casavis, fitsfile='test.UVFITS', field='3')
            self.assertTrue('SEVERE' in open('testlog.log').read())
        
        
    def test_antennaselect(self):
        '''
            test_antennaselect
            --------------------
            
            Check that the antenna selection specifies the antenna to include in the UVFITS file
            
            Also check that an invalid selection will not be allowed
        '''
        
        exportuvfits(casavis, fitsfile='test.UVFITS', antenna='')
        noselectsize = os.path.getsize('test.UVFITS')
        os.remove('test.UVFITS')
        
        exportuvfits(casavis, fitsfile='test.UVFITS', antenna='0~2')
        selectsize = os.path.getsize('test.UVFITS')
        self.assertTrue(os.path.exists('test.UVFITS'))
        os.remove('test.UVFITS')
        
        self.assertTrue(noselectsize > selectsize)
        if CASA6:
            with self.assertRaises(RuntimeError):
                exportuvfits(casavis, fitsfile='test.UVFITS', antenna='30')
        else:
            casalog.setlogfile('testlog.log')
            exportuvfits(casavis, fitsfile='test.UVFITS', antenna='30')
            self.assertTrue('SEVERE' in open('testlog.log').read())
        
    def test_timerange(self):
        '''
            test_timerange
            ----------------
            
            Check that the timerange parameter selectiom specifies the timeranges to include in the UVFITS file
            
            Also check that an invalid selection will not be allowed
        '''
        
        exportuvfits(casavis, fitsfile='test.UVFITS', timerange='')
        noselectsize = os.path.getsize('test.UVFITS')
        os.remove('test.UVFITS')
        
        exportuvfits(vis=casavis, fitsfile='test.UVFITS', timerange='09:18:45~09:24:45')
        selectsize = os.path.getsize('test.UVFITS')
        self.assertTrue(os.path.exists('test.UVFITS'))
        os.remove('test.UVFITS')
        
        self.assertTrue(noselectsize > selectsize)
        
        if CASA6:
            with self.assertRaises(RuntimeError):
                exportuvfits(casavis, fitsfile='test.UVFITS', timerange='99:99:98~99:99:99')
        
        self.assertFalse(os.path.exists('test.UVFITS'))
        
        casalog.setlogfile('testlog.log')
        if not CASA6:
            exportuvfits(casavis, fitsfile='test.UVFITS', timerange='99:99:98~99:99:99')
            self.assertTrue('SEVERE' in open('testlog.log').read())
        
        
    def test_datacolumn(self):
        '''
            test_datacolumn
            -----------------
            
            Check that the datacolumn selected with this parameter determines the data used when creating the UVFITS file
            
            The possible values of the datacolumn parameter noted in the documentation are 'data', 'corrected', or 'model'
            
        '''
        
        # Show that different selections for datacolumn produce different results
        # This isn't always the case though. For this file data and corrected are the same
        
        exportuvfits(modelvis, fitsfile='test.UVFITS', datacolumn='data')
        exportuvfits(modelvis, fitsfile='test2.UVFITS', datacolumn='model')
        
        self.assertFalse(filecmp.cmp('test.UVFITS','test2.UVFITS'))
        
        
    def test_multisource(self):
        '''
            test_multisource
            ------------------
            
            Check that this parameter determines that if the file is multi or single source file
            
            multi source files have multiple source IDs in the source (SU) table
        '''
        
        exportuvfits(othervis, fitsfile='test.UVFITS', multisource=True)
        exportuvfits(othervis, fitsfile='test2.UVFITS', multisource=False)
        
        self.assertFalse(filecmp.cmp('test.UVFITS','test2.UVFITS'))
        
        os.remove('test2.UVFITS')
        
    def test_combinespw(self):
        '''
            test_combinespw
            -----------------
            
            Check that this parameter exports the spectral windows as IFs
        
        '''
        
        exportuvfits(othervis, fitsfile='test.UVFITS', combinespw=True)
        exportuvfits(othervis, fitsfile='test2.UVFITS', combinespw=False)
        
        #os.remove('test2.UVFITS')
        
        self.assertFalse(filecmp.cmp('test.UVFITS','test2.UVFITS'))
        
        os.remove('test2.UVFITS')
        
    def test_writestation(self):
        '''
            test_writestation
            -------------------
            
            Check that the station name is written instead of the antenna name
        '''
        
        exportuvfits(casavis, fitsfile='test.UVFITS', writestation=True)
        hdul = fits.open('test.UVFITS')
        data = hdul[2].data
        self.assertTrue('VLA:N7' in data['ANNAME'])
        hdul.close()
        
        exportuvfits(casavis, fitsfile='test2.UVFITS', writestation=False)
        hdul = fits.open('test2.UVFITS')
        data = hdul[2].data
        self.assertTrue('VA01' in data['ANNAME'])
        hdul.close()
        
        os.remove('test.UVFITS')
        os.remove('test2.UVFITS')


        
    def test_overwrite(self):
        '''
            test_overwrite
            ----------------
            
            Check that this parameter allows you to overwrite an existing cal table
        '''
        
        casalog.setlogfile('testlog.log')
        
        exportuvfits(casavis, fitsfile='test.UVFITS', overwrite=False)
        exportuvfits(casavis, fitsfile='test.UVFITS', overwrite=True)
        
        if CASA6:
            with self.assertRaises(RuntimeError):
                exportuvfits(casavis, fitsfile='test.UVFITS', overwrite=False)
        else:
            exportuvfits(casavis, fitsfile='test.UVFITS', overwrite=False)
            self.assertTrue('SEVERE' in open('testlog.log').read())
        
    def test_padwithflags(self):
        '''
            test_padwithflags
            -------------------
            
            Check that missing data is filled with flags to fit IFs
            
        '''
        
        # This sometimes works but sometimes doesn't? I would have to pull this test for now
        
        shutil.copytree(othervis, copyvis)
        tempvis = 'temp.ms'
        flagdata(copyvis,spw='1')
        split(copyvis, outputvis=tempvis, datacolumn='data', timebin='630s')
        
        exportuvfits(tempvis, fitsfile='test.UVFITS', combinespw=True, padwithflags=True)
        if CASA6:
            with self.assertRaises(RuntimeError):
                exportuvfits(tempvis, fitsfile='test2.UVFITS', combinespw=True, padwithflags=False)
        else:
            casalog.setlogfile('testlog.log')
            exportuvfits(tempvis, fitsfile='test2.UVFITS', combinespw=True, padwithflags=False)
            self.assertTrue('SEVERE' in open('testlog.log').read())
            
        self.assertTrue(os.path.exists('test.UVFITS'))
            
        
    def test_weightcompute(self):
        '''
            test_weightcompute
            ---------------------
            
            Check that if there is no data in the WEIGHT_SPECTRUM column then the a weight will be calculated for each visibility
            
            use modelvis for this one
        '''
        
        # How do I test this? Where in the fits file is the weight stored, or is it used in some calculation for the data column

        casalog.setlogfile('testlog.log')
        exportuvfits(modelvis, fitsfile='test.UVFITS', datacolumn='weight')
        
        self.assertTrue('Unrecognized column weight' in open('testlog.log').read())
        
        
        
    
    
def suite():
    return[exportuvfits_test]

if __name__ == '__main__':
    unittest.main()
