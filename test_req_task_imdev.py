##########################################################################
# test_req_task_imdev.py
#
# Copyright (C) 2018
# Associated Universities, Inc. Washington DC, USA.
#
# This script is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# [Add the link to the JIRA ticket here once it exists]
#
# Based on the requirements listed in plone found here:
# https://casa.nrao.edu/casadocs-devel/stable/global-task-list/task_imdev/about
#
#
##########################################################################

CASA6 = False
try:
    import casatools
    from casatasks import imdev
    CASA6 = True
except ImportError:
    from __main__ import default
    from tasks import *
    from taskinit import *
import sys
import os
import unittest
import shutil
import numpy as np
from filecmp import dircmp

### DATA ###

if CASA6:
    datapath = casatools.ctsys.resolve('image/orion_tfeather.im')
    datapath2 = casatools.ctsys.resolve('image/ngc5921.clean.image')
    stokespath = casatools.ctsys.resolve('image/image_input_processor.im/')
    interppath = casatools.ctsys.resolve('image/f2h_quantile.im')

else:
    if os.path.exists(os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req'):
        datapath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/image/orion_tfeather.im'
        datapath2 = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/image/ngc5921.clean.image'
        stokespath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/image/image_input_processor.im/'
        interppath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/image/f2h_quantile.im/'
        
    else:
        datapath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/image/orion_tfeather.im'
        datapath2 = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/image/ngc5921.clean.image'
        stokespath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/image/image_input_processor.im/'
        interppath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/image/f2h_quantile.im/'
        
        
output = 'testimage.im'
output2 = 'testimage2.im'
output3 = 'testimage3.im'

class imdev_test(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        pass
    
    def setUp(self):
        if not CASA6:
            default(imdev)
            
    def tearDown(self):
        if os.path.exists(output):
            shutil.rmtree(output)
            
        if os.path.exists(output2):
            shutil.rmtree(output2)
            
        if os.path.exists(output3):
            shutil.rmtree(output3)
            
        if os.path.exists('testcopy.im'):
            shutil.rmtree('testcopy.im')
    
    @classmethod
    def tearDownClass(cls):
        pass
    
    
    def test_imagename(self):
        '''
            test_imagename
            ----------------
            
            Check that the imagename parameter selects the image to perfrom the task on
        '''
        
        self.assertTrue(imdev(imagename=datapath))
    
    def test_outfile(self):
        '''
            test_outfile
            --------------
            
            Check that the outfile parameter passes the name out the output image to be produced
        '''
        
        imdev(imagename=datapath, outfile=output)
        self.assertTrue(os.path.exists(output))
        
    def test_region(self):
        '''
            test_region
            -------------
            
            Check that the region parameter selects a different section than the default
        '''
        
        imdev(imagename=datapath, outfile=output)
        imdev(imagename=datapath, outfile=output2, region='circle[[5h35m21s, -5d24m12s], 10.0arcsec]')
        
        dcmp = dircmp(output, output2)
        self.assertTrue(len(dcmp.diff_files) > 0)
        
    def test_box(self):
        '''
            test_box
            ----------
            
            Check that the box parameter properly selects a rectangular region
        '''
        
        imdev(imagename=datapath, outfile=output)
        imdev(imagename=datapath, outfile=output2, box='0,0,50,50')
        
        dcmp = dircmp(output, output2)
        self.assertTrue(len(dcmp.diff_files) > 0)
    
    def test_chans(self):
        '''
            test_chans
            ------------
            
            Check that the chans parameter selects a different channel
        '''
        
        imdev(imagename=datapath2, outfile=output, chans='0')
        imdev(imagename=datapath2, outfile=output2, chans='1')
        
        dcmp = dircmp(output, output2)
        self.assertTrue(len(dcmp.diff_files) > 0)
        
    def test_stokes(self):
        '''
            test_stokes
            -------------
            
            NOTE: Need to find another data set with stokes options
            Come back to this one
        '''
        
        imdev(imagename=stokespath, outfile=output)
        imdev(imagename=stokespath, outfile=output2, stokes='I')
        
        dcmp = dircmp(output, output2)
        
        self.assertTrue(len(dcmp.diff_files) > 0)
        
        
    def test_mask(self):
        '''
            test_mask
            -----------
            
            Check that mask selection masks a portion of the original image
        '''
    
        datacopy = 'testcopy.im'
        shutil.copytree(datapath, datacopy)
        os.chmod(datacopy, 493)
        for root, dirs, files in os.walk(datacopy):
            for d in dirs:
                os.chmod(os.path.join(root, d), 493)
            for f in files:
                os.chmod(os.path.join(root, f), 493)
    
        imdev(imagename=datapath, outfile=output)
        imdev(imagename=datapath, outfile=output2, mask=datacopy)
        
        dcmp = dircmp(output, output2)
        self.assertTrue(len(dcmp.diff_files) > 0)
        
        
    def test_overwrite(self):
        '''
            test_overwrite
            ----------------
            
            Check that the overwrite parameter = True overwrites a file of the existing  and raises no error
        '''
        
        imdev(imagename=datapath, outfile=output)
        imdev(imagename=datapath, outfile=output, overwrite=True)
        
        
    def test_grid(self):
        '''
            test_grid
            -----------
            
            Check that the grid parameter changes the grid spacing
        '''
        
        imdev(imagename=datapath, outfile=output, xlength=10)
        imdev(imagename=datapath, outfile=output2, xlength=10, grid=[2,2])
        
        dcmp = dircmp(output, output2)
        self.assertTrue(len(dcmp.diff_files) > 0)
        
        
    def test_anchor(self):
        '''
            test_anchor
            -------------
            
            Check that this selects the anchor pixel position
        '''
        
        imdev(imagename=datapath, outfile=output, xlength=10, grid=[4,5])
        imdev(imagename=datapath, outfile=output2, xlength=10, grid=[4,5], anchor=[0,0])
        
        dcmp = dircmp(output, output2)
        
        self.assertTrue(len(dcmp.diff_files) > 0)
        
        
        
    def test_xlength(self):
        '''
            test_xlength
            --------------
            
            Check that this parameter sets the x coordinate length of the bos, or the diameter of the circle. Cirle is used if ylength is an empty string
        '''
        
        imdev(imagename=datapath, outfile=output)
        imdev(imagename=datapath, outfile=output2, xlength=10)
        imdev(imagename=datapath, outfile=output3, xlength=10, ylength=10)
        
        dcmp1 = dircmp(output, output2)
        dcmp2 = dircmp(output, output3)
        dcmp3 = dircmp(output2, output3)
        
        self.assertTrue(len(dcmp1.diff_files) > 0)
        self.assertTrue(len(dcmp2.diff_files) > 0)
        self.assertTrue(len(dcmp3.diff_files) > 0)
        
        
    def test_ylength(self):
        '''
            test_ylength
            --------------
            
            Check that this gives the y coordinate length of a box. This returns a different image than the default
        '''
        
        imdev(imagename=datapath, outfile=output)
        imdev(imagename=datapath, outfile=output2, ylength=10)
        
        dcmp = dircmp(output, output2)
        
        self.assertTrue(len(dcmp.diff_files) > 0)
        
        
    def test_interp(self):
        '''
            test_interp
            -------------
            
            Check that the use of different interpolation algorithms creates different image files
        '''
        
        imdev(imagename=interppath, outfile=output, interp='cubic', xlength='2pix', ylength='2pix', grid=[100,100])
        imdev(imagename=interppath, outfile=output2, interp='linear', xlength='2pix', ylength='2pix', grid=[100,100])
        
        dcmp = dircmp(output, output2)
        
        self.assertTrue(len(dcmp.diff_files) > 0)
        
        
    def test_stattype(self):
        '''
            test_stattype
            ----------------
        '''
        
        imdev(imagename=datapath, outfile=output)
        imdev(imagename=datapath, outfile=output2, stattype='median')
        
        dcmp = dircmp(output, output2)
        
        self.assertTrue(len(dcmp.diff_files) > 0)
        
        
    def test_statalg(self):
        '''
            test_statalg
            --------------
            
            Check that changing the stat alg from classic to chauenet produces a different image
        '''
        
        imdev(imagename=datapath, outfile=output, xlength=10)
        imdev(imagename=datapath, outfile=output2, xlength=10, statalg='chauvenet')
        
        dcmp = dircmp(output, output2)
        
        self.assertTrue(len(dcmp.diff_files) > 0)
    
    
    def test_zscore(self):
        '''
            test_zscore
            -------------
            
            statalg='chauvenet' expandable parameter
            Check that using the zscore parameter generates a different image
        '''
        
        imdev(imagename=datapath, outfile=output, xlength=10, statalg='chauvenet')
        imdev(imagename=datapath, outfile=output2, xlength=10, statalg='chauvenet', zscore=2)
        
        dcmp = dircmp(output, output2)
        
        self.assertTrue(len(dcmp.diff_files) > 0)
        
        
    def test_maxiter(self):
        '''
            test_maxiter
            --------------
            
            statalg='chavenet' expandable parameter
            Check that using the maxiter parameter generates a different image
        '''
        
        imdev(imagename=datapath, outfile=output, xlength=10, statalg='chavenet')
        imdev(imagename=datapath, outfile=output2, xlength=10, statalg='chauvenet', maxiter=2)
        
        dcmp = dircmp(output, output2)
        
        self.assertTrue(len(dcmp.diff_files) > 0)
        
        
        
def suite():
    return[imdev_test]

if __name__ == '__main__':
    unittest.main()
