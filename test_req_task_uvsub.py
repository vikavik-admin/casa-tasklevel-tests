##########################################################################
# test_req_task_uvsub.py
#
# Copyright (C) 2018
# Associated Universities, Inc. Washington DC, USA.
#
# This script is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# [Add the link to the JIRA ticket here once it exists]
#
# Based on the requirements listed in plone found here:
# https://casa.nrao.edu/casadocs-devel/stable/global-task-list/task_uvsub/about
#
#
##########################################################################

CASA6 = False
try:
    import casatools
    from casatasks import uvsub
    CASA6 = True
except ImportError:
    from __main__ import default
    from tasks import uvsub
    from taskinit import *
import sys
import os
import unittest
import shutil
import numpy as np
from filecmp import dircmp

### DATA ###

if CASA6:
    datapath = casatools.ctsys.resolve('visibilities/vla/ngc5921.ms/')

else:
    if os.path.exists(os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req'):
        datapath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/vla/ngc5921.ms/'
        
    else:
        datapath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/vla/ngc5921.ms/'
        
       
datacopy1 = 'test1.ms'
datacopy2 = 'test2.ms'
       
class uvsub_test(unittest.TestCase):
    
    
    def setUp(self):
        shutil.copytree(datapath, datacopy1)
        os.chmod(datacopy1, 493)
        for root, dirs, files in os.walk(datacopy1):
            for d in dirs:
                os.chmod(os.path.join(root, d), 493)
            for f in files:
                os.chmod(os.path.join(root, f), 493)
                
        shutil.copytree(datapath, datacopy2)
        os.chmod(datacopy2, 493)
        for root, dirs, files in os.walk(datacopy2):
            for d in dirs:
                os.chmod(os.path.join(root, d), 493)
            for f in files:
                os.chmod(os.path.join(root, f), 493)
    
    def tearDown(self):
        shutil.rmtree(datacopy1)
        shutil.rmtree(datacopy2)
    
    
    def test_sub(self):
        '''
            test_sub
            ----------
            
            Check that the task subtracts the MODEL_DATA column from the CORRECTED_DATA column
        '''
        
        uvsub(datacopy1)
        dcmp = dircmp(datacopy1, datacopy2)
        self.assertTrue(len(dcmp.diff_files) > 0)
        
    def test_reverse(self):
        '''
            test_reverse
            --------------
            
            Check that the reverse parameter = True provides different results from the default
        '''
        
        uvsub(datacopy1)
        uvsub(datacopy2, reverse=True)
        dcmp = dircmp(datacopy1, datacopy2)
        self.assertTrue(len(dcmp.diff_files) > 0)
        
    
    
def suite():
    return[uvsub_test]

if __name__ == '__main__':
    unittest.main()
