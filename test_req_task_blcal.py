##########################################################################
# test_req_task_blcal.py
#
# Copyright (C) 2018
# Associated Universities, Inc. Washington DC, USA.
#
# This script is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# [Add the link to the JIRA ticket here once it exists]
#
# Based on the requirements listed in plone found here:
# https://casa.nrao.edu/casadocs-devel/stable/global-task-list/task_blcal/about
#
#
##########################################################################

CASA6 = False
try:
    import casatools
    from casatasks import blcal, gaincal
    tb = casatools.table()
    CASA6 = True
except ImportError:
    from __main__ import default
    from tasks import blcal, gaincal
    from taskinit import *
import sys
import os
import unittest
import shutil
import numpy as np

### DATA ###

if CASA6:
    orig_datapath = casatools.ctsys.resolve('visibilities/alma/uid___X02_X3d737_X1_01_small.ms/')
    orig_datapath2 = casatools.ctsys.resolve('visibilities/alma/Itziar.ms/')
    orig_datapath3 = casatools.ctsys.resolve('visibilities/alma/uid___A002_X30a93d_X43e_small.ms/')
    orig_parangdata = casatools.ctsys.resolve('visibilities/vla/ngc5921.ms/')
    orig_tablecompdata = casatools.ctsys.resolve('visibilities/vla/gaincaltest2.ms')
    
    orig_gcalref = casatools.ctsys.resolve('caltables/gaincaltest2.ms.G0')

else:
    if os.path.exists(os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req'):
        orig_datapath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/alma/uid___X02_X3d737_X1_01_small.ms/'
        orig_datapath2 = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/alma/Itziar.ms/'
        orig_datapath3 = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/alma/uid___A002_X30a93d_X43e_small.ms/'
        orig_parangdata = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/vla/ngc5921.ms/'
        orig_tablecompdata = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/vla/gaincaltest2.ms/'
        
        orig_gcalref = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/caltables/gaincaltest2.ms.G0'
        
    else:
        orig_datapath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/alma/uid___X02_X3d737_X1_01_small.ms/'
        orig_datapath2 = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/alma/Itziar.ms/'
        orig_datapath3 = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/alma/uid___A002_X30a93d_X43e_small.ms/'
        orig_parangdata = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/vla/ngc5921.ms/'
        orig_tablecompdata = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/vla/gaincaltest2.ms/'
        
        orig_gcalref = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/caltables/gaincaltest2.ms.G0'
        
def getparam(caltable, colname='CPARAM'):
    ''' Open a caltable and get the provided column '''

    tb.open(caltable)
    outtable = tb.getcol(colname)
    tb.close()

    return outtable
        
        
def tableComp(table1, table2, cols=[], rtol=8e-7, atol=1e-8):
    ''' Compare two caltables '''

    tableVal1 = {}
    tableVal2 = {}

    tb.open(table1)
    colname1 = tb.colnames()

    for col in colname1:
        try:
            tableVal1[col] = tb.getcol(col)
        except RuntimeError:
            pass
    tb.close()

    tb.open(table2)
    colname2 = tb.colnames()

    for col in colname2:
        try:
            tableVal2[col] = tb.getcol(col)
        except RuntimeError:
            pass
    tb.close()

    truthDict = {}

    for col in tableVal1.keys():

        try:
            truthDict[col] = np.isclose(tableVal1[col], tableVal2[col], rtol=rtol, atol=atol)
        except TypeError:
            print(col, 'ERROR in finding truth value')
            casalog.post(message=col+': ERROR in determining the truth value')

    if len(cols) == 0:
        
        truths = [[x, np.all(truthDict[x] == True)] for x in truthDict.keys()]

    else:

        truths = [[x, np.all(truthDict[x] == True)] for x in cols]

    return np.array(truths)
        
def createCopy(original, copied):
    
        shutil.copytree(original, copied)
        os.chmod(copied, 493)
        for root, dirs, files in os.walk(copied):
            for d in dirs:
                os.chmod(os.path.join(root, d), 493)
            for f in files:
                os.chmod(os.path.join(root, f), 493)
                
def change_perms(path):
    os.chmod(path, 0o777)
    for root, dirs, files in os.walk(path):
        for d in dirs:
            os.chmod(os.path.join(root,d), 0o777)
        for f in files:
            os.chmod(os.path.join(root,f), 0o777)


calout = 'test.gcal'
calout2 = 'test2.gcal'

datapath = 'uid_copy.ms'
datapath2 = 'Itziar_copy.ms'
datapath3 = 'uidA_copy.ms'
parangdata = 'ngc_copy.ms'
tablecompdata = 'gaincaltest_copy.ms'

gcalref = 'gaincalG0_copy.G0'

class blcal_test(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        
        shutil.copytree(orig_datapath, datapath)
        shutil.copytree(orig_datapath2, datapath2)
        shutil.copytree(orig_datapath3, datapath3)
        shutil.copytree(orig_parangdata, parangdata)
        shutil.copytree(orig_tablecompdata, tablecompdata)
        shutil.copytree(orig_gcalref, gcalref)
        
        change_perms(datapath)
        change_perms(datapath2)
        change_perms(datapath3)
        change_perms(parangdata)
        change_perms(tablecompdata)
        change_perms(gcalref)
        
        gaincal(vis=datapath2, caltable='table.gcal')
    
    def setUp(self):
        if not CASA6:
            default(blcal)
            
    def tearDown(self):
        if os.path.exists(calout):
            shutil.rmtree(calout)
            
        if os.path.exists(calout2):
            shutil.rmtree(calout2)
            
        if os.path.exists('table2.gcal'):
            shutil.rmtree('table2.gcal')
    
    @classmethod
    def tearDownClass(cls):
        if os.path.exists('table.gcal'):
            shutil.rmtree('table.gcal')
            
        shutil.rmtree(datapath)
        shutil.rmtree(datapath2)
        shutil.rmtree(datapath3)
        shutil.rmtree(parangdata)
        shutil.rmtree(tablecompdata)
        shutil.rmtree(gcalref)
    
    def test_caltable(self):
        ''' Check that the caltable param specifies the name of the output calibration table '''
        
        blcal(vis=datapath, caltable=calout)
        self.assertTrue(os.path.exists(calout))
        
    def test_field(self):
        ''' Check that the field parameter selects a subset of the data '''
        
        blcal(vis=datapath, field='1', caltable=calout)
        tb.open(calout)
        fields = tb.getcol('FIELD_ID')
        tb.close()
        
        self.assertTrue((fields == 1).all())
        
    def test_spw(self):
        ''' Check that the spw parameter selects a subset of the data '''
        
        blcal(vis=datapath, spw='1', caltable=calout)
        tb.open(calout)
        spws = tb.getcol('SPECTRAL_WINDOW_ID')
        tb.close()
        
        self.assertTrue((spws == 1).all())
        
    def test_intent(self):
        ''' Check that the intent parameter selects a subset of the data '''

        blcal(vis=datapath, intent='*AMPLI*', caltable=calout)
        
        callen = len(getparam(calout, 'TIME'))
        
        self.assertTrue(callen == 12)
        
    def test_scan(self):
        ''' Check that the scan parameter selects a subset of the data '''
        
        blcal(vis=datapath, scan='1', caltable=calout)
        tb.open(calout)
        scans = tb.getcol('SCAN_NUMBER')
        tb.close()
        
        self.assertTrue((scans == 1).all())
        
    def test_selectdata(self):
        ''' Check that the select data parameter allows for the use of additional selection parameters '''
        
        blcal(vis=datapath, scan='1', selectdata=False, caltable=calout)
        tb.open(calout)
        scans = tb.getcol('SCAN_NUMBER')
        tb.close()
        
        self.assertFalse((scans == 1).all())
        
    def test_timerange(self):
        ''' Check that the timerange parameter selects a subset of the data '''
        
        blcal(vis=datapath, timerange='3:07:51~3:08:22', caltable=calout)
        
        lentimerange = len(getparam(calout, 'TIME'))
        
        self.assertTrue(lentimerange == 12)
        
    def test_uvrange(self):
        ''' Check that the uvrange parameter selects a subset of the data '''
        
        if CASA6:
            try:
                blcal(vis=datapath, uvrange='1000~2000klambda', caltable=calout)
            except RuntimeError:
                self.assertFalse(os.path.exists(calout))
                                 
        else:
            blcal(vis=datapath, uvrange='1000~2000klambda', caltable=calout)
            self.assertFalse(os.path.exists(calout))
        
        
        
    def test_antenna(self):
        ''' Check that the antenna parameter selects a subset of the data '''
        
        blcal(vis=tablecompdata, caltable=calout, antenna='0~5&')
        flagged = np.sum(getparam(calout, 'FLAG'))
        
        self.assertTrue(flagged == 6720)
        
    def test_observation(self):
        ''' Check that the observation parameter selects a subset of the data '''
        
        blcal(vis=datapath2, observation='1', caltable=calout)
        tb.open(calout)
        obs = tb.getcol('OBSERVATION_ID')
        tb.close()
        
        self.assertTrue((obs == 1).all())
        
    def test_solint(self):
        ''' Checks that the solint parameter provides the solution interval '''
        
        blcal(vis=tablecompdata, solint='10s',caltable=calout)
        arraylen = len(getparam(calout, 'TIME'))
        
        self.assertTrue(arraylen == 138600)
        
    def test_combine(self):
        ''' Check that combine passes which axes to combine solve '''
        
        blcal(vis=datapath, combine='field, spw, scan', caltable=calout)
        withCombine = len(getparam(calout, 'TIME'))
        
        
        self.assertFalse(withCombine == 55)
        
    def test_freqdep(self):
        ''' Check that freqdep = True returns a different result than the default. This solves for frequency dependent solutions '''
        
        blcal(vis=tablecompdata, caltable=calout)
        blcal(vis=tablecompdata, freqdep=True, caltable=calout2)
        
        self.assertFalse(np.all(tableComp(calout, calout2)[:,1] == 'True'))
        
    def test_calmode(self):
        ''' Check that changing the calmode determines the type of solution '''
        
        blcal(vis=datapath, caltable=calout)
        blcal(vis=datapath, calmode = 'p', caltable=calout2)
        
        self.assertFalse(np.all(tableComp(calout, calout2)[:,1] == 'True'))
        
    def test_solnorm(self):
        ''' normalizes the average solution amplitudes to 1.0 '''
        
        blcal(vis=datapath, caltable=calout)
        blcal(vis=datapath, solnorm=True, caltable=calout2)
        
        noNorm = getparam(calout, 'CPARAM')
        withNorm = getparam(calout2, 'CPARAM')
        
        self.assertFalse(np.all(noNorm == withNorm))
        
    def test_gaintable(self):
        ''' Check that the gaintable parameter takes gain calibrations to apply on the fly '''
        
        blcal(vis=datapath2, caltable=calout)
        blcal(vis=datapath2, gaintable=['table.gcal'], caltable=calout2)
        
        self.assertFalse(np.all(tableComp(calout, calout2)[:,1] == 'True'))
        
    def test_gainfield(self):
        ''' Check that the gainfield parameter selects a subset of the parameters '''
    
        blcal(vis=datapath2, gaintable=['table.gcal'], caltable=calout)
        blcal(vis=datapath2, gaintable=['table.gcal'], gainfield=['1'],caltable=calout2)
        
        self.assertFalse(np.all(tableComp(calout, calout2)[:,1] == 'True'))
        
    def test_interp(self):
        ''' provides the interpolation mode to use for each gaintable '''
        
        blcal(vis=datapath2, gaintable=['table.gcal'], caltable=calout)
        blcal(vis=datapath2, gaintable=['table.gcal'], interp=['cubic'], caltable=calout2)
        
        self.assertFalse(np.all(tableComp(calout, calout2)[:,1] == 'True'))
        
    def test_spwmap(self):
        ''' Check that giving an spwmap returns a different table than the default with gaintable. This parameter selects spectral window combinations '''
        
        blcal(vis=tablecompdata, gaintable=[gcalref], caltable=calout)
        blcal(vis=tablecompdata, gaintable=[gcalref], spwmap=[0,0,1,1], caltable=calout2)
        
        self.assertFalse(np.all(tableComp(calout, calout2)[:,1] == 'True'))
        
    def test_parang(self):
        ''' Check that setting the parang parameter to true has a different affect than false. This parameter applies parallactic angle corrections when true '''
        
        blcal(vis=parangdata, caltable=calout)
        blcal(vis=parangdata, parang=True, caltable=calout2)
        
        withoutParang = getparam(calout)
        withPrang = getparam(calout2)
        
        self.assertFalse(np.all(withoutParang == withPrang))
        
    
def suite():
    return[blcal_test]

if __name__ == '__main__':
    unittest.main()
