##########################################################################
# test_req_task_importfits.py
#
# Copyright (C) 2018
# Associated Universities, Inc. Washington DC, USA.
#
# This script is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# [Add the link to the JIRA ticket here once it exists]
#
# Based on the requirements listed in plone found here:
# https://casa.nrao.edu/casadocs-devel/stable/global-task-list/task_importfits/about
#
#
##########################################################################

CASA6 = False
try:
    import casatools
    from casatasks import importfits, rmtables, casalog, imhead
    tb = casatools.table()
    CASA6 = True
    from astropy.io import fits
except ImportError:
    from __main__ import default
    from tasks import *
    from taskinit import *
    import pyfits as fits
import sys
import os
import unittest
import shutil
import numpy as np


### DATA ###

if CASA6:
    fitsfile = casatools.ctsys.resolve('fits/1904-66_AIR.fits')
else:
    fitsfile = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/fits/1904-66_AIR.fits'
    
casaimage = 'test.image'
logname = 'testlog.log'
logpath = casalog.logfile()


class importfits_test(unittest.TestCase):
    
    def setUp(self):
        if not CASA6:
            default(importfits)
            
    def tearDown(self):
        casalog.setlogfile(logpath)
        if os.path.exists(casaimage):
            rmtables(casaimage)
        if os.path.exists(logname):
            os.remove(logname)
    
    
    def test_makesFile(self):
        '''
            test_makesFile
            ------------------
            
            Test that the task will create a CASA image from a fits file
        '''
        
        importfits(fitsimage=fitsfile, imagename=casaimage)
        
        self.assertTrue(os.path.exists(casaimage))
        
    def test_whichRep(self):
        '''
            test_whichRep
            ----------------
            
            Test that whichrep parameter selection allows you to use different coordinate representations if the fits image allows.
            NOTE: Need a better dataset, check for the coordinates directly
        '''
        casalog.setlogfile(logname)
        
        outfile = 'outfile.fits'
        
        hdu = fits.open(fitsfile)
        hdu[0].header['CTYPE1A'] = 'TLON-CAR'
        hdu[0].header['CTYPE2A'] = 'TLAT-CAR'
        hdu[0].header['CUNIT1A'] = 'deg'
        hdu[0].header['CUNIT2A'] = 'deg'
        
        hdu.writeto(outfile)
        
        importfits(fitsimage=outfile, imagename=casaimage, whichrep=1)
        
        self.assertTrue('exceeds the number available' not in open(logname).read())
        
        os.remove(outfile)
        
    def test_whichHdu(self):
        '''
            test_whichHdu
            ----------------
            
            Test that hdu change the header data unit if the file contains multiple images
            NOTE: Need a fits file with multiple images
        '''
        
        outfile = 'concat.fits'
        new_hdul = fits.HDUList()
        new_hdul.append(fits.ImageHDU(np.zeros((300,300))))
        new_hdul.append(fits.ImageHDU(np.zeros((300,300))))
        
        #new_hdul.writeto(outfile, overwrite=True)
        new_hdul.writeto(outfile)
        
        importfits(fitsimage='concat.fits', imagename=casaimage, whichhdu=1)
        
        os.remove(outfile)
        
        
        
    def test_default(self):
        '''
            test_default
            ---------------
            
            Test that defaultaxis and defaultaxesvalues will fill missing axes with provided values as degenerate axes
        '''
        
        importfits(fitsimage=fitsfile, imagename=casaimage, defaultaxes=True, defaultaxesvalues=['','','88GHz','Q'])
        axes = imhead(casaimage)['axisnames']
        
        self.assertTrue(all(np.array([item in ['Right Ascension', 'Declination', 'Stokes', 'Frequency'] for item in axes])))
    
    
    def test_beam(self):
        '''
            test_default
            --------------
            
            Test that the beam parameter allows you to add or change existing beam parameters
            taken in form [BMAJ, BMIN, BPA]
        '''
        
        importfits(fitsimage=fitsfile, imagename=casaimage, beam=['.3arcsec','.2arcsec','.25deg'])
        beamComp = imhead(casaimage)['restoringbeam']
        
        self.assertTrue(['arcsec', .3] == [beamComp['major']['unit'], beamComp['major']['value']])
        self.assertTrue(['arcsec', .2] == [beamComp['minor']['unit'], beamComp['minor']['value']])
        self.assertTrue(['deg', .25] == [beamComp['positionangle']['unit'], beamComp['positionangle']['value']])
        
    def test_zeroblanks(self):
        '''
            test_zeroblanks
            -----------------
            
            Test that blanked pixels are NaN when zeroblanks=False and replaced with zero when True
        '''
        
        importfits(fitsimage=fitsfile, imagename=casaimage, zeroblanks=False)
        tb.open(casaimage)
        self.assertTrue(np.isnan(tb.getcol('map')[0,0,0]))
        tb.close()
        
        importfits(fitsimage=fitsfile, imagename='test2.image', zeroblanks=True)
        tb.open('test2.image')
        self.assertTrue(0.0 == tb.getcol('map')[0,0,0])
        tb.close()
        rmtables('test2.image')
        
    def test_overwrite(self):
        '''
            test_overwrite
            ----------------
            
            Test that the overwrite parameter will overwrite existing casa images
        '''
        
        importfits(fitsimage=fitsfile, imagename=casaimage)
        importfits(fitsimage=fitsfile, imagename=casaimage, overwrite=True)
        
    
    
    
    
def suite():
    return[importfits_test]

if __name__ == '__main__':
    unittest.main()
    
