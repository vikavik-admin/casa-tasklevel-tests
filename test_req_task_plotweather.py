##########################################################################
# test_req_task_plotweather.py
#
# Copyright (C) 2018
# Associated Universities, Inc. Washington DC, USA.
#
# This script is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# [Add the link to the JIRA ticket here once it exists]
#
# Based on the requirements listed in plone found here:
# https://casa.nrao.edu/casadocs-devel/stable/global-task-list/task_plotweather/about
#
# Test_mesSet: Check that only a valid MS is taken
# Test_removescr: Check that the scratch column is removed
# Test_removeotf: Check to make sure the virtual model is removed
# Test-removefield: Check that only the selected fields are removed (This part is broken)
#
##########################################################################

CASA6 = False
try:
    import casatools
    from casatasks import casalog, plotweather, rmtables
    CASA6 = True
    tb = casatools.table()
except ImportError:
    from __main__ import default
    from tasks import *
    from taskinit import *
import sys
import os
import unittest
import shutil
import numpy as np
import matplotlib.pyplot as plt
# Testing image comp with cv2
from skimage.measure import compare_ssim as ssim

### WARNING: This task is not yet implemented in CASA 6 ###

# DATA #
if CASA6:
    datapath = casatools.ctsys.resolve('visibilities/evla/refim_Cband.G37line.ms')
    wrongdata = casatools.ctsys.resolve('visibilities/alma/uid___X02_X3d737_X1_01_small.ms')
    impath = casatools.ctsys.resolve('digitalimage/refim_Cband.G37line.ms.plotweather.png')
    noWeather = casatools.ctsys.resolve('visibilities/alma/Itziar.ms')
else:
    if os.path.exists(os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/evla/refim_Cband.G37line.ms'):
        datapath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/evla/refim_Cband.G37line.ms'
        wrongdata = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/alma/uid___X02_X3d737_X1_01_small.ms'
        impath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/digitalimage/refim_Cband.G37line.ms.plotweather.png'
        noWeather = os.environ.get('CASAPATH').split()[0] + ('/data/casa-data-req/visibilities/alma/Itziar.ms')
    else:
        datapath = os.environ.get('CASAPATH').split()[0] + '/data/regression/unittest/clean/refimager/refim_Cband.G37line.ms'
        wrongdata = os.environ.get('CASAPATH').split()[0] + '/data/regression/unittest/listobs/uid___X02_X3d737_X1_01_small.ms'
        # No path for this png in current data dir
        impath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/digitalimage/refim_Cband.G37line.ms.plotweather.png'
        noWeather = os.environ.get('CASAPATH').split()[0] + ('/data/casa-data-req/visibilities/alma/Itziar.ms')
        
logpath = casalog.logfile()
path = 'refim_Cband.G37line.ms'

class plotweather_test(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        shutil.copytree(datapath, path)
    
    def setUp(self):
        if not CASA6:
            default(plotweather)
    
    def tearDown(self):
        casalog.setlogfile(logpath)
    
    @classmethod
    def tearDownClass(cls):
        rmtables(path)
        
    def test_VLAonly(self):
        '''test VLAonly: Check that plot weather only functions when given VLA data'''
        # Check that VLA/EVLA data doesn't raise any errors
        # For some reason this breaks if you are using the version from the datapath
        self.assertTrue(type(plotweather(path)) == list, msg='A ms that used VLA/EVLA data failed to execute')
        
        # Check that non-VLA data raises a severe error
        self.assertIsInstance(plotweather(wrongdata), type(None))
        
        # Check that it won't accept a fake ms
        self.assertIsInstance(plotweather('fake.ms'), type(None))
        os.remove(path + '.plotweather.png')
    
    def test__makesPlot(self):
        '''test makesPlot: Check that a plot is made and saved using the specified default naming scheme'''
        plotweather(path, doPlot=True)
        self.assertTrue(os.path.exists(path + '.plotweather.png'))
        os.remove(path + '.plotweather.png')
    
    def test_estOpacity(self):
        # This test might not be needed
        # I don't know wehre the model data is at for the Tau_Z figure
        pass
    
    def test_seasonalWeights(self):
        '''test seasonalWeights: Check that the seasonal weights affects the opacity in the expected manner'''
        standard = plotweather(path)[0]
        upper = plotweather(path, seasonal_weight=0.7)[0]
        lower = plotweather(path, seasonal_weight=0.3)[0]
        self.assertTrue(lower < standard < upper, msg='Seasonal weight does not operate in the expected manner')
        os.remove(path + '.plotweather.png')
    
    def test_savedName(self):
        '''test savedName: Check that files are saved with the provided unique name'''
        plotweather(path, plotName='testing.png')
        # Check that the image has the new provided name
        self.assertTrue(os.path.exists('testing.png'))
        os.remove('testing.png')
    
    def test_fileType(self):
        '''test fileType: Check that files can be saved with specified extensions'''
        # Check png
        self.assertTrue(type(plotweather(path, plotName='test.png')) == list)
        os.remove('test.png')
        # Check pdf
        self.assertTrue(type(plotweather(path, plotName='test.pdf')) == list)
        os.remove('test.pdf')
        # Check ps
        self.assertTrue(type(plotweather(path, plotName='test.ps')) == list)
        os.remove('test.ps')
        # Check eps
        self.assertTrue(type(plotweather(path, plotName='test.eps')) == list)
        os.remove('test.eps')
        # Check svg
        self.assertTrue(type(plotweather(path, plotName='test.svg')) == list)
        os.remove('test.svg')
        # Check that a different type fails
        self.assertFalse(type(plotweather(path, plotName='test.txt')) == list)
    
    def test_funct(self):
        # I need more information on what
        pass
    
    def test_hasWeather(self):
        '''Test hasWeather: Check that the ms that works has the weather dir and the other does not'''
        self.assertTrue(os.path.exists(path + '/WEATHER'))
        self.assertFalse(os.path.exists(noWeather + '/WEATHER'))
        
    def test_imageComp(self):
        '''Test imageComp: Check that the image generated is the same as some reference image that was generated'''
        plotweather(path, plotName='comp.png')
        shutil.copyfile(impath, 'imfile.png')
        #generated = cv2.imread('comp.png')
        #compared = cv2.imread('imfile.png')
        
        #generated = cv2.cvtColor(generated, cv2.COLOR_BGR2GRAY)
        #compared = cv2.cvtColor(compared, cv2.COLOR_BGR2GRAY)
        
        generated = plt.imread('comp.png')
        compared = plt.imread('imfile.png')
        
        s = ssim(generated, compared, multichannel=True)
        
        #diff = np.sum((generated.astype("float") - compared.astype("float")) ** 2)
        #diff /= float(generated.shape[0] * compared.shape[1])
        
        # not sure why there is a difference here Should I allow for this difference, or whould there be none/
        # could it be some sort of timestamp or something?

        self.assertTrue(s == 1.00)
        
        os.remove('comp.png')
        os.remove('imfile.png')
        
    
def suite():
    return[plotweather_test]

if __name__ == '__main__':
    unittest.main()
    
