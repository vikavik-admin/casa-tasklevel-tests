##########################################################################
# test_req_task_bandpass.py
#
# Copyright (C) 2018
# Associated Universities, Inc. Washington DC, USA.
#
# This script is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# [Add the link to the JIRA ticket here once it exists]
#
# Based on the requirements listed in plone found here:
# https://casa.nrao.edu/casadocs-devel/stable/global-task-list/task_bandpass/about
#
#
##########################################################################

CASA6 = False
try:
    import casatools
    from casatasks import bandpass, casalog
    CASA6 = True
    tb = casatools.table()
except ImportError:
    from __main__ import default
    from tasks import *
    from taskinit import *
import os
import unittest
import shutil
import numpy as np
import pylab as pl

if CASA6:
    datapath = casatools.ctsys.resolve('visibilities/vla/gaincaltest2.ms')
    compCal = casatools.ctsys.resolve('caltables/gaincaltest2.ms.G0')
    tCal = casatools.ctsys.resolve('caltables/gaincaltest2.ms.T0')
    # Reference Cals
    infref = casatools.ctsys.resolve('caltables/bandpassrefinf.B0')
    ref50s = casatools.ctsys.resolve('caltables/bandpassref50s.B0')
    refcombine = casatools.ctsys.resolve('caltables/combineSpw.B0')
    refnorm = casatools.ctsys.resolve('caltables/bandpassnorm.B0')
    refbandtypeB = casatools.ctsys.resolve('caltables/bandtypeB.B0')
    refbandtypeBPOLY = casatools.ctsys.resolve('caltables/bandtypeBPOLY.B0')
    refsmodel = casatools.ctsys.resolve('caltables/bandpasssmodel.B0')
    reffillgap = casatools.ctsys.resolve('caltables/bandgaps.B0')
    refpreapply = casatools.ctsys.resolve('caltables/preapply.B0')
    refcallib = casatools.ctsys.resolve('text/refcallib.txt')
    refgainfield = casatools.ctsys.resolve('caltables/gainfield.B0')
    refinterp = casatools.ctsys.resolve('caltables/bandpassinterp.B0')
    refspwmap = casatools.ctsys.resolve('caltables/bandpassspwmap.B0')
    refparang = casatools.ctsys.resolve('caltables/bandpassparang.B0')

    
else:
    if os.path.exists(os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req'):
        datapath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/vla/gaincaltest2.ms'
        compCal = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/caltables/gaincaltest2.ms.G0'
        tCal = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/caltables/gaincaltest2.ms.T0'
        # Reference Cals
        infref = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/caltables/bandpassrefinf.B0'
        ref50s = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/caltables/bandpassref50s.B0'
        refcombine = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/caltables/combineSpw.B0'
        refnorm = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/caltables/bandpassnorm.B0'
        refbandtypeB = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/caltables/bandtypeB.B0'
        refbandtypeBPOLY = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/caltables/bandtypeBPOLY.B0'
        refsmodel = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/caltables/bandpasssmodel.B0'
        reffillgap = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/caltables/bandgaps.B0'
        refpreapply = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/caltables/preapply.B0'
        refcallib = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/text/refcallib.txt'
        refgainfield = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/caltables/gainfield.B0'
        refinterp = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/caltables/bandpassinterp.B0'
        refspwmap = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/caltables/bandpassspwmap.B0'
        refparang = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/caltables/bandpassparang.B0'
        
    else:
        datapath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/vla/gaincaltest2.ms'
        compCal = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/caltables/gaincaltest2.ms.G0'
        tCal = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/caltables/gaincaltest2.ms.T0'
        # Reference Cals
        infref = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/caltables/bandpassrefinf.B0'
        ref50s = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/caltables/bandpassref50s.B0'
        refcombine = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/caltables/combineSpw.B0'
        refnorm = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/caltables/bandpassnorm.B0'
        refbandtypeB = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/caltables/bandtypeB.B0'
        refbandtypeBPOLY = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/caltables/bandtypeBPOLY.B0'
        refsmodel = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/caltables/bandpasssmodel.B0'
        reffillgap = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/caltables/bandgaps.B0'
        refpreapply = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/caltables/preapply.B0'
        refcallib = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/text/refcallib.txt'
        refgainfield = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/caltables/gainfield.B0'
        refinterp = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/caltables/bandpassinterp.B0'
        refspwmap = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/caltables/bandpassspwmap.B0'
        refparang = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/caltables/bandpassparang.B0'
        
def getparam(caltable, colname='CPARAM'):
    ''' Open a caltable and get the provided column '''

    tb.open(caltable)
    outtable = tb.getcol(colname)
    tb.close()

    return outtable


def tableComp(table1, table2, cols=[], rtol=8e-7, atol=1e-8):
    ''' Compare two caltables '''

    tableVal1 = {}
    tableVal2 = {}

    tb.open(table1)
    colname1 = tb.colnames()

    for col in colname1:
        try:
            tableVal1[col] = tb.getcol(col)
        except RuntimeError:
            pass
    tb.close()

    tb.open(table2)
    colname2 = tb.colnames()

    for col in colname2:
        try:
            tableVal2[col] = tb.getcol(col)
        except RuntimeError:
            pass
    tb.close()

    truthDict = {}

    for col in tableVal1.keys():

        try:
            truthDict[col] = np.isclose(tableVal1[col], tableVal2[col], rtol=rtol, atol=atol)
        except TypeError:
            print(col, 'ERROR in finding truth value')
            casalog.post(message=col+': ERROR in determining the truth value')

    if len(cols) == 0:
        
        truths = [[x, np.all(truthDict[x] == True)] for x in truthDict.keys()]

    else:

        truths = [[x, np.all(truthDict[x] == True)] for x in cols]

    return np.array(truths)

tempcal = 'tempcal.cal'
bandpassinf = 'infcal.B0'
bandpass50s = '50scal.B0'
bandpasscomb = 'combinetest.B0'
bandpassnorm = 'normtest.B0'
bandtypeB = 'bandtypeB.B0'
temptCal = 'temptcal.T0'


class bandpass_test(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        if not CASA6:
            default(bandpass)
    
    def setUp(self):
        pass
    
    def tearDown(self):
        if os.path.exists(tempcal):
            shutil.rmtree(tempcal)
        if os.path.exists(bandpassnorm):
            shutil.rmtree(bandpassnorm)
        if os.path.exists(bandpasscomb):
            shutil.rmtree(bandpasscomb)
        if os.path.exists(bandpass50s):
            shutil.rmtree(bandpass50s)
        if os.path.exists(bandpassinf):
            shutil.rmtree(bandpassinf)
        if os.path.exists(temptCal):
            shutil.rmtree(temptCal)
    
    @classmethod
    def tearDownClass(cls):
        pass
    
    def test_correctGains(self):
        '''
            test_corrtectGains
            ---------------------
            
            Compare the gaintable to a Reference cal table and make sure they produce the same results
        '''
        
        bandpass(vis=datapath, caltable=bandpassinf, solint='inf', field='0', scan='0~9', refant='0', smodel=[1,0,0,0])
        
        self.assertTrue(np.all(tableComp(bandpassinf, infref)[:,1] == 'True'))
    
    def test_SNRonInterval(self):
        '''
            test_SNRonInterval
            --------------------
            
            Check that as the solution interval decreases the signal to noise also decreases
        '''
        bandpass(vis=datapath, caltable=bandpassinf, solint='inf', field='0', scan='0~9', refant='0', smodel=[1,0,0,0])
        bandpass(vis=datapath, caltable=bandpass50s, solint='50s', field='0', scan='0~9', refant='0', smodel=[1,0,0,0])
        
        SNRinf = np.mean(getparam(bandpassinf, 'SNR'))
        SNR50s = np.mean(getparam(bandpass50s, 'SNR'))
        
        self.assertTrue(SNR50s < SNRinf)
        
    def test_fieldSelect(self):
        '''
            test_fieldSelect
            ------------------
            
            Check that the field selection parameter properly selects a subset of the data
        '''
        
        bandpass(vis=datapath, caltable=bandpassinf, solint='inf', field='0', scan='0~9', refant='0', smodel=[1,0,0,0])
        
        fields = getparam(bandpassinf, 'FIELD_ID')
        
        self.assertTrue(np.all(fields == 0))
        
    def test_spwSelect(self):
        '''
           test_spwSelect
           ----------------
           
           Check that the spw selection parameter properly selects a subset of the data
        '''
        
        bandpass(vis=datapath, caltable=tempcal, solint='inf', spw='2', field='0', refant='0', smodel=[1,0,0,0])
        spws = getparam(tempcal, 'SPECTRAL_WINDOW_ID')
        
        self.assertTrue(np.all(spws == 2))
        
    def test_selectData(self):
        '''
            test_selectData
            -----------------
            
            Check that the select data parameter allows for the use of additional selection parameters
        '''
        
        bandpass(vis=datapath, caltable=tempcal, solint='inf', selectdata=False, scan='3', field='0', refant='0', smodel=[1,0,0,0])
        scans = getparam(tempcal, 'SCAN_NUMBER')
        
        self.assertTrue(np.all(scans != 3))
        
    def test_timerange(self):
        '''
            test_timerange
            ----------------
            
            Check that the timerange selection parameter properly specifies a subsection of the data
        '''
        
        bandpass(vis=datapath, caltable=tempcal, solint='inf', timerange='04:33:23~04:38:23', smodel=[1,0,0,0], refant='0')
        timelen = getparam(tempcal, 'TIME')
        
        self.assertTrue(len(timelen) == 40)
        
        
    def test_antennaSelect(self):
        '''
            test_antennaSelect
            --------------
            
            Check that all antennas outside of the selection are flagged by bandpass
        '''
        
        bandpass(vis=datapath, caltable=tempcal, solint='inf', smodel=[1,0,0,0], refant='0', antenna='0~5&')
        flagged = np.sum(getparam(tempcal, 'FLAG'))
        
        self.assertTrue(flagged == 5376)
        
    def test_scanSelect(self):
        '''
            test_scanSelect
            -----------------
            
            Check that the scan selection parameter properly selects a subset of the data
        '''
        
        bandpass(vis=datapath, caltable=tempcal, solint='inf', smodel=[1,0,0,0], refant='0', scan='4')
        scans = getparam(tempcal, 'SCAN_NUMBER')
    
        self.assertTrue(np.all(scans == 4))
        
    def test_solint(self):
        '''
            test_solint
            -------------
            
            Check that the solint parmeter determines the time length of the solution interval
        '''
        bandpass(vis=datapath, caltable=bandpass50s, solint='50s', field='0', scan='0~9', refant='0', smodel=[1,0,0,0])
        
        self.assertTrue(np.all(tableComp(bandpass50s, ref50s)[:,1] == 'True'))
        
    def test_combine(self):
        '''
            test_combine
            --------------
            
            Check that the combine parameter properly averages over the provided axis
        '''
        
        bandpass(vis=datapath, caltable=bandpasscomb, solint='inf', refant='0', combine='scan, spw', smodel=[1,0,0,0])
        
        self.assertTrue(np.all(tableComp(bandpasscomb, refcombine)[:,1] == 'True'))
        
    def test_minblperant(self):
        '''
            test_minblperant
            ------------------
            
            Check that solutions made with fewer baselines than this limit are not accepted
        '''
        
        bandpass(vis=datapath, caltable=tempcal, refant='0', solint='inf', smodel=[1,0,0,0], minblperant=6, antenna='0~5&')
        
        self.assertFalse(os.path.exists(tempcal))
        
    def test_minSNR(self):
        '''
            test_minSNR
            -------------
            
            Check that solutions with a SNR lower than the provided limit are flagged
        '''
            
        bandpass(vis=datapath, caltable=tempcal, refant='0', solint='inf', smodel=[1,0,0,0], minsnr=1000)
        flags = np.sum(getparam(tempcal, 'FLAG'))
        
        self.assertTrue(flags == 13440)
        
    def test_solnorm(self):
        '''
            test_solnorm
            --------------
            
            Check that a normalized bandpass calibration matches a reference cal table
        '''
        
        bandpass(vis=datapath, caltable=bandpassnorm, solint='inf', refant='0', smodel=[1,0,0,0], solnorm=True)
        
        self.assertTrue(np.all(tableComp(bandpassnorm, refnorm)[:,1] == 'True'))
        
    def test_bandtypeB(self):
        '''
            test_bandtypeB
            ----------------
            
            Check that band type B does a channel by channel solution for each specified spw
        '''
        
        bandpass(vis=datapath, caltable=tempcal, solint='inf', refant='0', smodel=[1,0,0,0], bandtype='B')
        
        self.assertTrue(np.all(tableComp(tempcal, refbandtypeB)[:,1] == 'True'))
        
    def test_bandtypeBPOLY(self):
        '''
            test_bandtypeBPOLY
            --------------------
            
            Check that band type BPOLY fits an nth order polynomial for the amplitude and phase
        '''
        
        bandpass(vis=datapath, caltable=tempcal, solint='inf', refant='0', smodel=[1,0,0,0], bandtype='BPOLY')
        
        tb.open(tempcal)
        print(len(tb.getcol('TIME')))
        tb.close()
        
        print(tableComp(tempcal, refbandtypeBPOLY))
        
        self.assertTrue(np.all(tableComp(tempcal, refbandtypeBPOLY)[:,1] == 'True'))
        
    
    def test_smodel(self):
        '''
            test_smodel
            -------------
            
            Check that the smodel sets the point Stokes parameter correctly
            Compare this output to a reference calibration table
        '''
        
        bandpass(vis=datapath, caltable=tempcal, solint='inf', refant='0', smodel=[2,0,0,0])
        
        self.assertTrue(np.all(tableComp(tempcal, refsmodel)[:,1] == 'True'))
    
    
    def test_append(self):
        '''
            test_append
            -------------
            
            Check that the append parameter adds to an existing cal table
        '''
        
        bandpass(vis=datapath, caltable=tempcal, solint='inf', refant='0', smodel=[1,0,0,0])
        beforelen = len(getparam(tempcal, 'TIME'))
        
        bandpass(vis=datapath, caltable=tempcal, solint='inf', refant='0', smodel=[1,0,0,0], append=True)
        afterlen = len(getparam(tempcal, 'TIME'))
        
        self.assertTrue(beforelen < afterlen)
        
    
    def test_fillgaps(self):
        '''
            test_fillgaps
            ---------------
            
            Check that the fill gaps output results in a cal table identical to the reference cal table
        '''
        
        bandpass(vis=datapath, caltable=tempcal, solint='inf', refant='0', smodel=[1,0,0,0], fillgaps=5)
        
        self.assertTrue(np.all(tableComp(tempcal, reffillgap)[:,1] == 'True'))
        
    
    def test_docallib(self):
        '''
            test_docallib
            ---------------
            
            Check the callib properly pre applys a caltable
        '''
        
        shutil.copytree(tCal, temptCal)
        
        bandpass(vis=datapath, caltable=tempcal, solint='inf', refant='0', smodel=[1,0,0,0], docallib=True, callib=refcallib)
        
        self.assertTrue(np.all(tableComp(tempcal, refpreapply)[:,1] == 'True'))
        
        
    def test_gaintable(self):
        '''
            test_gaintable
            ----------------
            
            Check that pre-applying a caltable using the gaintable parameter matches a reference calibration table
        '''
        
        bandpass(vis=datapath, caltable=tempcal, solint='inf', refant='0', smodel=[1,0,0,0], gaintable=[tCal])
        
        self.assertTrue(np.all(tableComp(tempcal, refpreapply)[:,1] == 'True'))
        
        
        
    def test_gainfield(self):
        '''
            test_gainfield
            ----------------
            
            Check that a subset of the fields from the gaintable are selected by comparison to reference table values
        '''
        
        bandpass(vis=datapath, caltable=tempcal, solint='inf', refant='0', smodel=[1,0,0,0], gaintable=[tCal], gainfield='0')
        
        self.assertTrue(np.all(tableComp(tempcal, refgainfield)[:,1] == 'True'))
        
        
    def test_interp(self):
        '''
            test_interp
            -------------
            
            Check that changing the interp parameter results in a table identical to a reference calibration table
        '''
        
        bandpass(vis=datapath, caltable=tempcal, solint='inf', refant='0', smodel=[1,0,0,0], gaintable=[tCal], interp='cubic')
        
        self.assertTrue(np.all(tableComp(tempcal, refinterp)[:,1] == 'True'))
        
        
    def test_spwmap(self):
        '''
            test_spwmap
            
            Check that the caltable solutions are mapped to the designated spectral window
        '''
        
        bandpass(vis=datapath, caltable=tempcal, solint='inf', refant='0', smodel=[1,0,0,0], gaintable=[tCal], spwmap=[0,0,0,0])
        
        self.assertTrue(np.all(tableComp(tempcal, refspwmap)[:,1] == 'True'))
        
        
    def test_parang(self):
        '''
            test_parang
            -------------
            
            Check that parallactic angle correction is applied
        '''
        
        bandpass(vis=datapath, caltable=tempcal, solint='inf', refant='0', smodel=[1,0,0,0], parang=True)
        
        self.assertTrue(np.all(tableComp(tempcal, refparang)[:,1] == 'True'))
        
    def test_uvrange(self):
        '''
            test_uvrange
            --------------
            
            Check that the uvrange parameter selects only data within the provided range
        '''
        
        bandpass(vis=datapath, caltable=tempcal, solint='inf', refant='0', smodel=[1,0,0,0], uvrange='0~100klambda')
        
        self.assertFalse(os.path.exists(tempcal))
    
    
def suite():
    return[bandpass_test]


if __name__ == '__main__':
    unittest.main()
