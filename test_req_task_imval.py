########################################################################
# test_req_task_imval.py
#
# Copyright (C) 2018
# Associated Universities, Inc. Washington DC, USA
#
# This script is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# [Add the link to the JIRA ticket here once it exists]
#
# Based on the requirements listed in plone found here:
# https://casa.nrao.edu/casadocs-devel/stable/global-task-list/task_imval/about
#
#
##########################################################################

CASA6 = False
try:
    import casatools
    from casatasks import imval, casalog, imhead, imstat
    CASA6 = True
except ImportError:
    from __main__ import default
    from tasks import *
    from taskinit import *
    import casaTestHelper
import sys
import os
import unittest
import shutil
import numpy
import casaTestHelper

if CASA6:
    casaimagepath = casatools.ctsys.resolve('image/ngc5921.clean.image')
    fitspath = casatools.ctsys.resolve('fits/1904-66_AIR.fits')
    stokestestimagepath = casatools.ctsys.resolve('image/image_input_processor.im')
else:
    if os.path.exists(os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req'):
        dataroot = os.environ.get('CASAPATH').split()[0] + '/'
        casaimagepath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/image/ngc5921.clean.image'
        fitspath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/fits/1904-66_AIR.fits'
        stokestestimagepath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/image/image_input_processor.im'
    else:
        dataroot = os.environ.get('CASAPATH').split()[0] + '/'
        casaimagepath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/image/ngc5921.clean.image'
        fitspath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/fits/1904-66_AIR.fits'
        stokestestimagepath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/image/image_input_processor.im'
        
logpath = casalog.logfile()

test_dict = {}

class imval_test(unittest.TestCase):
    
    def setUp(self):
        ''' Generate region files on the fly to use for testing '''
        file = open("ngc5921.image.crtf", "w")
        file.write("#CRTFv0 \n box[[5pix, 7pix], [20pix, 22pix]]")
        file.close()
        
        file = open("ngc5921.image_complex.crtf", "w")
        file.write("#CRTFv0 \n box[[5pix, 7pix], [20pix, 22pix]] \n box[[18pix, 20pix], [40pix, 45pix]]")
        file.close()
        
        file = open("ngc5921.image_ellipse.crtf", "w")
        file.write("#CRTFv0 \n ellipse[[20pix, 20pix], [10pix, 5pix], 90deg]")
        file.close()
        
        if not CASA6:
            default(imval)
        else:
            pass
            
    def tearDown(self):
        casalog.setlogfile(logpath)
        if os.path.exists('testlog.log'):
            os.remove('testlog.log')
        casaTestHelper.generate_weblog("imval", test_dict)
        
    def test_checkOutput(self):
        ''' 1. test_checkOutput: Tests if imval returns a data structure which holds blc, trc,
        axes, units, data, mask, and coords '''
        results = imval(casaimagepath, box='5,10,5,10')
        blc = results['blc']
        trc = results['trc']
        axes = results['axes']
        unit = results['unit']
        data = results['data']
        mask = results['mask']
        coords = results['coords']
        report = blc and trc and axes and unit and data.any() and mask.any() and coords.any()
        self.assertTrue(report)
        
    def test_takesRegionFile(self):
        ''' 2. test_takesRegionFile: tests if imval takes a region file as input '''
        results = imval(casaimagepath, region="ngc5921.image.crtf")
        self.assertTrue(results)
        
    def test_complexRegionInput(self):
        ''' 3. test_complexRegion: Tests if imval returns data for only the first simple region 
        if a complex region is specified '''
        results = imval(casaimagepath, region="ngc5921.image_complex.crtf")
        blc = results['blc']
        trc = results['trc']
        self.assertTrue(blc[0] == 5 and blc[1] == 7 and trc[0] == 20 and trc[1] ==22)
        
    def test_boundingBoxRegion(self):
        ''' 4. test_boundingBoxRegion: Tests if imval returns the region defined by the bounding
        box of a non-rectangular region '''
        results = imval(casaimagepath, region="ngc5921.image_ellipse.crtf")
        blc = results['blc']
        trc = results['trc']
        self.assertTrue(blc[0] == 10 and blc[1] == 15 and trc[0] == 30 and trc[1] == 25)
        
    def test_emptyRegionValue(self):
        ''' 5. test_emptyRegionValue: if the region parameter is undefined, the inputs to the 
        box/chans/stokes parameters will be used to define the region '''
        results = imval(casaimagepath, box='0,0,0,45', chans='1', stokes='I')
        blc = results['blc']
        trc = results['trc']
        print(blc)
        print(trc)
        
    def test_negativeOneBox(self):
        ''' 6. test_negativeOneBox: if box=-1, the details for all pixels in the selected 
        frequency and stokes planes will be returned '''
        results = imval(casaimagepath, box="-1")
        blc = results['blc']
        print(blc)
        print(type(blc))
        trc = results['trc']
        print(trc)
        print(type(trc))
        imstatresults = imstat(imagename=casaimagepath)
        imstatBlc = imstatresults['blc']
        print(imstatBlc)
        print(type(imstatBlc))
        imstatTrc = imstatresults['trc']
        print(imstatTrc)
        print(type(imstatTrc))
        print(blc == imstatBlc.tolist() )
        print(trc == imstatTrc.tolist())
        self.assertTrue(blc == imstatBlc.tolist() and trc == imstatTrc.tolist())
    
    @casaTestHelper.stats_dict(test_dict)
    def test_emptyBoxParameter(self):
        ''' 7. test_emptyBoxParameter: if the box parameter is empty, the details of
        the direction plane reference pixel will be returned. '''
        results = imval(casaimagepath, box="")
        blc = results['blc']
        trc = results['trc']
        print(results['trc'])
        print(results['blc'])
        self.assertTrue(blc[0] == trc[0] and blc[1] == trc[1])
        report = (blc[0] == trc[0]) and (blc[1] == trc[1])
        casaTestHelper.add_to_dict(self, output=test_dict, data_set="ngc5921.clean.image", result=report)
        
    def test_emptyChansParameter(self):
        ''' 8. test_emptyChansParameter: if the chans parameter is empty, all channels will be used. '''
        results1 = imval(casaimagepath, region="ngc5921.image.crtf", chans='2')
        print(results1["axes"])
        data1 = results1["data"]
        print(type(results1["mask"]))
        mask1 = results1["mask"]
        #print(results["unit"])
        results2 = imval(casaimagepath, region="ngc5921.image.crtf")
        print(type(results2["mask"]))
        #print(results["unit"])
        data2 = results2["data"]
        mask2 = results2["mask"]
        print(data1.all() == data2.all())
        print(numpy.array_equal(mask1, mask2))
        self.assertTrue(data1.all() == data2.all() and numpy.array_equal(mask1, mask2))
        
    @casaTestHelper.stats_dict(test_dict)
    def test_emptyStokesParameter(self):
        ''' 9. test_emptyStokesParameter: if the stokes parameter is empty, all Stokes planes will be used '''
        results = imval(stokestestimagepath, region="ngc5921.image.crtf", stokes='')
        data = results["data"]
        print(data.shape)
        
    @casaTestHelper.stats_dict(test_dict)
    def test_specifiedChannelParameter(self):
        ''' 10. test_specifiedChannelParameter: return data for a specified channel or channels '''
        results = imval(casaimagepath, region="ngc5921.image.crtf", chans='1')
        data = results["data"]
        print(data.shape)
        
    @casaTestHelper.stats_dict(test_dict)
    def test_specifiedStokesParameter(self):
        ''' 11. test_specifiedStokesParamater: return data for specified Stoke plane or planes '''
        results = imval(stokestestimagepath, region="ngc5921.image.crtf", stokes='I')
        data = results["data"]
        print(data.shape)
        
            
def suite():
    return[imval_test]
        
# Main #
if __name__ == '__main__':
    unittest.main()